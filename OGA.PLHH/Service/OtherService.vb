Imports System.IO
Imports System.Data

Public Class OtherService


    Public Function getPathWSOther() As String
        Try
            If File.Exists("BarcodeConfig.xml") = False Then
                Return "NoConfigFile"

                'Dim ds As New DataSet
                'Dim dt As New DataTable("TableConfig")
                'With dt
                '    .Columns.Add(New DataColumn("ConfigValue", GetType(String)))
                'End With
                'Dim dr As DataRow
                'dr = dt.NewRow
                'dr("ConfigValue") = String.Empty 'GlobalVariable.WSOtherURL
                'dt.Rows.Add(dr)
                'dt.AcceptChanges()
                'ds.Tables.Add(dt)
                'ds.WriteXml("BarcodeConfig.xml")
                'ds.Dispose()
                'GlobalVariable.WSOtherURL = "http://172.31.210.197/WSOther/OtherService.asmx"
            Else
                Dim ds As New DataSet
                ds.ReadXml("BarcodeConfig.xml")

                If ds.Tables(0).Rows.Count <> 0 Then
                    GlobalVariable.WSOtherURL = ds.Tables(0).Rows(0).Item("ConfigValue").ToString
                End If
                ds.Dispose()
            End If
            Return True
        Catch ex As Exception
            Throw
        End Try
    End Function
End Class
