Public Class InstanceService

    Public Shared Function SetNetworkCredential() As System.Net.WebProxy
        If Not IsNumeric(GlobalVariable.WSPort) Then
            WSPort = "80"
        End If
        Dim pr As New System.Net.WebProxy(GlobalVariable.WSProxyURL, CInt(WSPort))
        Dim cr As New System.Net.NetworkCredential(GlobalVariable.WSUsername, _
                                                    GlobalVariable.WSPassword, _
                                                    GlobalVariable.WSDomain)
        pr.Credentials = cr
        Return pr
    End Function

    ''' <summary>
    ''' ��Ǩ�ͺ��� ����ö�Դ��� Server ��������� �µԴ��ʹ��� URL web service other ����˹������ xml file ������ͧ
    ''' </summary>
    Public Shared Function IsCanConnentOnline(Optional ByVal newURL As String = "") As Boolean
        Try
            If newURL = "" Then
                Dim wsOther As WSOther.OtherService = InstanceService.GetWSOtherInstance
                Dim result As Boolean
                result = wsOther.IsCanConnentWS()
                Return result
            Else
                Dim wsOther As WSOther.OtherService = InstanceService.GetWSOtherInstance(newURL)
                Dim result As Boolean
                result = wsOther.IsCanConnentWS()
                Return result
            End If

        Catch ex As System.Net.WebException
            MsgSystem.DisconnectServer(ex.Message)
            Return False
        Catch ex As Exception
            Throw New Exception(ex.ToString)
            Return False
        End Try
        Return True
    End Function

#Region "Set web service URL, Time out, HH Version"
    ''' <summary>
    ''' ��˹���� URL Web Service �����ҡ Server ��������á�ҧ
    ''' </summary>
    Public Shared Sub SetWSInitialValue(ByVal ds As Data.DataSet)
        If ds IsNot Nothing Then
            If ds.Tables.Count > 0 Then
                Dim dv As Data.DataView = ds.Tables(0).DefaultView

                dv.RowFilter = "ConfigCode='HHWSConsohTimeout'"
                If dv.Count > 0 Then
                    GlobalVariable.WSTimeOut = dv(0)("ConfigValue").ToString()
                End If

                dv.RowFilter = "ConfigCode='HHVersion'"
                If dv.Count > 0 Then
                    GlobalVariable.CurrentVersion = dv(0)("ConfigValue").ToString()
                End If

                dv.RowFilter = "ConfigCode='HHWSConsohGR'"
                If dv.Count > 0 Then
                    GlobalVariable.WSGRURL = dv(0)("ConfigValue").ToString()
                End If
                dv.RowFilter = "ConfigCode='HHWSConsohGT'"
                If dv.Count > 0 Then
                    GlobalVariable.WSGTURL = dv(0)("ConfigValue").ToString()
                End If
                dv.RowFilter = "ConfigCode='HHWSConsohGI'"
                If dv.Count > 0 Then
                    GlobalVariable.WSGIURL = dv(0)("ConfigValue").ToString()
                End If
                dv.RowFilter = "ConfigCode='HHWSConsohCO'"
                If dv.Count > 0 Then
                    GlobalVariable.WSCOURL = dv(0)("ConfigValue").ToString()
                End If
                dv.RowFilter = "ConfigCode='HHWSConsohPutaway'"
                If dv.Count > 0 Then
                    GlobalVariable.WSPutaway = dv(0)("ConfigValue").ToString()
                End If
                dv.RowFilter = ""
            End If
        End If
    End Sub
#End Region

#Region "Get Web Service Instance"
    ''' <summary>
    ''' ���ҧ object web service : OtherService
    ''' </summary>
    Public Shared Function GetWSOtherInstance(Optional ByVal newURL As String = "") As WSOther.OtherService
        Dim ws As New WSOther.OtherService
        If newURL = "" Then
            If String.IsNullOrEmpty(GlobalVariable.WSOtherURL) Then
                'OGAMsg.ShowInformation("��辺������ URL" & vbCrLf & "Web Service OtherService.asmx")
            Else
                If WSIsUseProxy Then
                    ws.Proxy = SetNetworkCredential()
                End If
                ws.Url = GlobalVariable.WSOtherURL
                ws.Timeout = GlobalVariable.WSTimeOut
            End If
        Else
            If String.IsNullOrEmpty(GlobalVariable.WSOtherURL(newURL)) Then
                'OGAMsg.ShowInformation("��辺������ URL" & vbCrLf & "Web Service OtherService.asmx")
            Else
                If WSIsUseProxy Then
                    ws.Proxy = SetNetworkCredential()
                End If
                ws.Url = GlobalVariable.WSOtherURL(newURL)
                ws.Timeout = GlobalVariable.WSTimeOut
            End If
        End If

        Return ws
    End Function

    ''' <summary>
    ''' ���ҧ object web service : GRService
    ''' </summary>
    Public Shared Function GetWSGRInstance() As WSGR.GRService

        Dim ws As New WSGR.GRService
        If String.IsNullOrEmpty(GlobalVariable.WSGRURL) Then
            'OGAMsg.ShowInformation("��辺������ URL Web Service GRService.asmx")
        Else
            If WSIsUseProxy Then
                ws.Proxy = SetNetworkCredential()
            End If
            ws.Url = GlobalVariable.WSGRURL
            ws.Timeout = GlobalVariable.WSTimeOut
        End If
        Return ws
    End Function


    ''' <summary>
    ''' ���ҧ object web service : GIService
    ''' </summary>
    Public Shared Function GetWSGIInstance() As WSGI.GIService
        Dim ws As New WSGI.GIService
        If String.IsNullOrEmpty(GlobalVariable.WSGIURL) Then
            'OGAMsg.ShowInformation("��辺������ URL Web Service GIService.asmx")
        Else
            If WSIsUseProxy Then
                ws.Proxy = SetNetworkCredential()
            End If
            ws.Url = GlobalVariable.WSGIURL
            ws.Timeout = GlobalVariable.WSTimeOut
        End If
        Return ws
    End Function

#End Region
   

    

   
End Class
