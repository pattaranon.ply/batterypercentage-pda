Imports System.Data
Module GlobalVariable

    Public matCode, batch, weight, loc, scanDate, remark As String



    Private _UserName As String

    Private _ProgramVersion As String

    Private _Environment As String

    Public dtAuthentication As DataTable
    'Public dtGTIM As DataTable
    'Public dtGTIMScan As DataTable
    'Public dtGTIMToWM As DataTable

    Public dsGR As DataSet
    Public dsGI As DataSet
    Public dsOrder As DataSet

    Public dsScanGR As DataSet '

    Public dsCount As DataSet
    Public dsCountDetail As DataSet

    'Public InitIPServer As String = "172.23.112.2" 'Server Surinomya
    'Public InitUrlWS As String = "http://172.23.112.2/WSBarcode/OtherService.asmx"

    ' Public InitIPServer As String = "172.23.112.2" 'Server Surinomya
    Public InitUrlWS As String = "http://IPServer/Virtual/OtherService.asmx"

    Public POXmlPath As String = "PODataPDA.xml"
    Public POScanXmlPath As String = "POScanPDA.xml"
    Public CountDetailXMLPath As String = "CountDetail.xml"
    Public CountScanXMLPath As String = "CountScan.xml"


    'Public InitIPServer As String = "172.23.112.250" 'For Test


    Private _ConnectionStatus As String


    Private _UserFullName As String
    Public Property UserFullName() As String
        Get
            Return _UserFullName
        End Get
        Set(ByVal value As String)
            _UserFullName = value
        End Set
    End Property


    Public Property ConnectionStatus() As String
        Get
            Return _ConnectionStatus
        End Get
        Set(ByVal value As String)
            _ConnectionStatus = value
        End Set
    End Property


    Private _IPServer As String
    Public Property IPServer() As String
        Get
            Return _IPServer
        End Get
        Set(ByVal value As String)
            _IPServer = value
        End Set
    End Property


    Private _SettingType As String

    Public Property SettingType() As String
        Get
            Return _SettingType
        End Get
        Set(ByVal value As String)
            _SettingType = value
        End Set
    End Property


    Private _DateNowServer As String

    Public Property DateNowServer() As String
        Get
            Return _DateNowServer
        End Get
        Set(ByVal value As String)
            _DateNowServer = value
        End Set
    End Property


    ''' <summary>
    ''' �Ţ��� Version �Ѩ�غѹ�ͧ����� ��� Device ����ͧ��� Install
    ''' </summary>
    Public Property ProgramVersion() As String
        Get
            Return _ProgramVersion
        End Get
        Set(ByVal value As String)
            _ProgramVersion = value
        End Set
    End Property

    ''' <summary>
    ''' ����-���ʡ�ż����ҹ�к� ��ҡ��� Login
    ''' </summary>
    Public Property UserName() As String
        Get
            Return _UserName
        End Get
        Set(ByVal value As String)
            _UserName = value
        End Set
    End Property


#Region "URL Web Service"
    '�Ӣ������Ҩҡ XML ����˹��������������� Handheld
    Private _WSOtherURL As String

    Public Property WSOtherURL(Optional ByVal newURL As String = "") As String
        Get
            If newURL = "" Then
                'Return String.Format("http://{0}/WSBarcode/OtherService.asmx", GlobalVariable.IPServer)
                ' Return String.Format("http://{0}/WSOtherRack/OtherService.asmx", GlobalVariable.IPServer)
                Return _WSOtherURL
            Else
                ' Return String.Format("http://{0}/WSBarcode/OtherService.asmx", newIP)
                Return newURL
            End If

        End Get
        Set(ByVal value As String)
            _WSOtherURL = value
        End Set
    End Property

    Private _WSPutaway As String

    Public Property WSPutaway() As String
        Get
            Return _WSPutaway
        End Get
        Set(ByVal value As String)
            _WSPutaway = value
        End Set
    End Property


    Private _WSGRURL As String
    Public Property WSGRURL() As String
        Get
            Return _WSGRURL '"http://172.20.76.247/WSGR/GRService.asmx" '
        End Get
        Set(ByVal value As String)
            _WSGRURL = value
        End Set
    End Property

    Private _WSGTURL As String
    Public Property WSGTURL() As String
        Get
            Return _WSGTURL
        End Get
        Set(ByVal value As String)
            _WSGTURL = value
        End Set
    End Property

    Private _WSGIURL As String
    Public Property WSGIURL() As String
        Get
            Return _WSGIURL
        End Get
        Set(ByVal value As String)
            _WSGIURL = value
        End Set
    End Property

    Private _WSCountURL As String
    Public Property WSCountURL() As String
        Get
            Return _WSCountURL
        End Get
        Set(ByVal value As String)
            _WSCountURL = value
        End Set
    End Property

    Private _WSCOURL As String
    Public Property WSCOURL() As String
        Get
            Return _WSCOURL
        End Get
        Set(ByVal value As String)
            _WSCOURL = value
        End Set
    End Property

    Public Property Environment() As String
        Get
            Return _Environment
        End Get
        Set(ByVal value As String)
            _Environment = value
        End Set
    End Property
#End Region

    Private _CurrentVersion As String
    Public Property CurrentVersion() As String
        Get
            Return _CurrentVersion
        End Get
        Set(ByVal value As String)
            _CurrentVersion = value
        End Set
    End Property

#Region "Connection Setting"

    Private _WSTimeOut As Integer
    Public Property WSTimeOut() As Integer
        Get
            Return 400000 '
        End Get
        Set(ByVal value As Integer)
            _WSTimeOut = value
        End Set
    End Property

    Private _WSProxyURL As String
    Public Property WSProxyURL() As String
        Get
            Return _WSProxyURL
        End Get
        Set(ByVal value As String)
            _WSProxyURL = value
        End Set
    End Property

    Private _WSPort As String
    Public Property WSPort() As String
        Get
            Return _WSPort
        End Get
        Set(ByVal value As String)
            _WSPort = value
        End Set
    End Property

    Private _WSUsername As String
    Public Property WSUsername() As String
        Get
            Return _WSUsername
        End Get
        Set(ByVal value As String)
            _WSUsername = value
        End Set
    End Property

    Private _WSPassword As String
    Public Property WSPassword() As String
        Get
            Return _WSPassword
        End Get
        Set(ByVal value As String)
            _WSPassword = value
        End Set
    End Property

    Private _WSDomain As String
    Public Property WSDomain() As String
        Get
            Return _WSDomain
        End Get
        Set(ByVal value As String)
            _WSDomain = value
        End Set
    End Property

    Private _WSIsUseProxy As Boolean
    Public Property WSIsUseProxy() As Boolean
        Get
            Return _WSIsUseProxy
        End Get
        Set(ByVal value As Boolean)
            _WSIsUseProxy = value
        End Set
    End Property
#End Region


End Module

