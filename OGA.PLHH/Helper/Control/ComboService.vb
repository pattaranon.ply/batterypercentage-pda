Imports System.Data

Public Class ComboService

    Public Shared Sub BindComboData(ByRef cboTarget As ComboBox, ByVal dt As DataTable, _
           ByVal strTextField As String, ByVal strValueField As String, _
           Optional ByVal strDefaultValue As String = "")
        Try
            cboTarget.DataSource = Nothing
            cboTarget.Items.Clear()
            If dt.Rows.Count <> 0 Then
                Dim dr As DataRow = dt.NewRow()
                With cboTarget
                    cboTarget.DataSource = Nothing
                    .BeginUpdate()
                    .DataSource = dt
                    .DisplayMember = strTextField
                    .ValueMember = strValueField
                    .EndUpdate()
                    .SelectedIndex = -1
                    If strDefaultValue <> "" Then
                        .SelectedValue = strDefaultValue
                    End If
                End With
            Else
                cboTarget.Items.Add("- - - - No Data - - - -")
                cboTarget.SelectedIndex = 0
            End If
        Catch ex As Exception
            OGAMsg.ShowErrorTryCatch("Show data for Combo Box error !", ex)
        End Try
    End Sub

    Public Shared Sub BindComboData(ByRef cboTarget As ComboBox, ByVal dv As DataView, _
           ByVal strTextField As String, ByVal strValueField As String, _
           Optional ByVal strDefaultValue As String = "")
        Try
            cboTarget.DataSource = Nothing
            cboTarget.Items.Clear()
            If dv.Count <> 0 Then
                With cboTarget
                    cboTarget.DataSource = Nothing
                    .BeginUpdate()
                    .DataSource = dv
                    .DisplayMember = strTextField
                    .ValueMember = strValueField
                    .EndUpdate()
                    .SelectedIndex = -1
                    If strDefaultValue <> "" Then
                        .SelectedValue = strDefaultValue
                    End If
                End With
            Else
                cboTarget.Items.Add("- - - - No Data - - - -")
                cboTarget.SelectedIndex = 0
            End If
        Catch ex As Exception
            OGAMsg.ShowErrorTryCatch("Show data for Combo Box error !", ex)
        End Try
    End Sub

    Public Shared Sub BindComboDataSelectAll(ByRef cboTarget As ComboBox, ByVal dt As DataTable, _
          ByVal strTextField As String, ByVal strValueField As String, _
          Optional ByVal strDefaultValue As String = "", Optional ByVal strDisplayTextAll As String = "")
        Try
            cboTarget.DataSource = Nothing
            cboTarget.Items.Clear()
            If dt.Rows.Count <> 0 Then
                Dim dr As DataRow

                dr = dt.NewRow()
                dr(strValueField) = "ALL"
                If strDisplayTextAll <> "" Then
                    dr(strTextField) = strDisplayTextAll
                Else
                    dr(strTextField) = ""
                End If


                dt.Rows.InsertAt(dr, 0)
                dt.AcceptChanges()
                With cboTarget
                    cboTarget.DataSource = Nothing
                    .BeginUpdate()
                    .DataSource = dt
                    .DisplayMember = strTextField
                    .ValueMember = strValueField
                    .EndUpdate()
                    .SelectedIndex = -1
                    If strDefaultValue <> "" Then
                        .SelectedValue = strDefaultValue
                    End If
                End With
            Else
                cboTarget.Items.Add("- - - - - No Data - - - - -")
                cboTarget.SelectedIndex = 0
            End If
        Catch ex As Exception
            OGAMsg.ShowErrorTryCatch("Show data for Combo Box error !", ex)
        End Try
    End Sub

End Class
