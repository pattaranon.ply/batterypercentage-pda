Imports System.Collections.Generic
Imports System.Text
Imports System.Drawing

Public Delegate Sub CheckCellEventHandler(ByVal sender As Object, ByVal e As DataGridEnableEventArgs)
Public Class DataGridEnableEventArgs
    Inherits EventArgs
    Private _row As Integer
    Private _meetsCriteria As Boolean

    Public Sub New(ByVal row As Integer, ByVal val As Boolean)
        _row = row
        _meetsCriteria = val
    End Sub

    Public Property Row() As Integer
        Get
            Return _row
        End Get
        Set(ByVal value As Integer)
            _row = value
        End Set
    End Property

    Public Property MeetsCriteria() As Boolean
        Get
            Return _meetsCriteria
        End Get
        Set(ByVal value As Boolean)
            _meetsCriteria = value
        End Set
    End Property

    Protected Overrides Sub Finalize()
        MyBase.Finalize()
    End Sub

    Private Shared fieldCon As String = String.Empty
    Private Shared valuefield As String = String.Empty
    Private Shared dtCon As New Data.DataTable
    Private Shared Sub myStyle_CheckCellContains(ByVal sender As Object, ByVal e As DataGridEnableEventArgs)
        Try

            If dtCon.Rows(e.Row)(fieldCon) = valuefield Then
                cColor.Color = Color.Tomato
                e.MeetsCriteria = True
            Else
                cColor.Color = Color.Green
                e.MeetsCriteria = False
            End If
            'e.MeetsCriteria = false;
        Catch generatedExceptionName As Exception
            Throw
        End Try
    End Sub
    Private Shared IsAmount As Boolean
    Private Shared Sub myStyle_CheckCellContainsAmount(ByVal sender As Object, ByVal e As DataGridEnableEventArgs)
        Try
            If (IsAmount = False) Then
                If dtCon.Rows(e.Row)(fieldCon) <> dtCon.Rows(e.Row)(valuefield) Then
                    cColor.Color = Color.Tomato
                    e.MeetsCriteria = True
                Else
                    cColor.Color = Color.Green
                    e.MeetsCriteria = False
                End If
            Else
                If dtCon.Rows(e.Row)(fieldCon) = dtCon.Rows(e.Row)(valuefield) Then
                    cColor.Color = Color.Tomato
                    e.MeetsCriteria = True
                Else
                    cColor.Color = Color.Green
                    e.MeetsCriteria = False
                End If
            End If

            'e.MeetsCriteria = false;
        Catch generatedExceptionName As Exception
            Throw
        End Try
    End Sub
    ''' <summary>
    ''' Function ����¹�� DataGridTableStyle
    ''' ������¡�� Ex:DataGridEnableEventArgs.addGridStyle(dg,dtSye,dtTableSyrly,"FieldName","Y")
    ''' </summary>
    ''' <param name="dg">DataGrid</param>
    ''' <param name="dtSye">DataTable:DataSource</param>
    ''' <param name="dtTableStyle">DataGridTableStyle DataGrid</param>
    ''' <param name="field">FieldName:Condition</param>
    ''' <param name="value">Condition</param>
    ''' <remarks>������¡�� Ex:DataGridEnableEventArgs.addGridStyle(dg,dtSye,dtTableSyrly,"FieldName","Y")</remarks>
    Public Shared Sub addGridStyle(ByRef dg As DataGrid, ByVal dtSye As Data.DataTable, ByVal dtTableStyle As System.Windows.Forms.DataGridTableStyle, ByVal field As String, ByVal value As String)
        Try
            If dtSye.Columns.Count <= 0 Then Exit Sub
            If dtTableStyle.GridColumnStyles.Count <= 0 Then Exit Sub

            fieldCon = field
            valuefield = value
            dtCon = dtSye
            Dim dtStyle As New DataGridTableStyle()
            dtStyle.MappingName = dtSye.TableName
            Dim dgColumn(dtSye.Columns.Count) As ColumnStyle
            For i As Integer = 0 To dtTableStyle.GridColumnStyles.Count - 1

                dgColumn(i) = New ColumnStyle(i)
                'dgColumn(i).Format = ""
                dgColumn(i).HeaderText = dtTableStyle.GridColumnStyles.Item(i).HeaderText
                dgColumn(i).MappingName = dtTableStyle.GridColumnStyles.Item(i).MappingName
                dgColumn(i).Width = dtTableStyle.GridColumnStyles.Item(i).Width
                dgColumn(i).Site = dtTableStyle.GridColumnStyles.Item(i).Site
                dgColumn(i).PropertyDescriptor = dtTableStyle.GridColumnStyles.Item(i).PropertyDescriptor
                AddHandler dgColumn(i).CheckCellEven, AddressOf myStyle_CheckCellContains
                dtStyle.GridColumnStyles.Add(dgColumn(i))
            Next

            dtStyle.MappingName = dtSye.TableName

            dg.TableStyles.Clear()
            dg.TableStyles.Add(dtStyle)
        Catch ex As Exception
            Throw ex
        End Try

    End Sub

    ''' <summary>
    ''' Function ����¹�� DataGridTableStyle
    ''' ������¡�� Ex:DataGridEnableEventArgs.addGridStyle(dg,dtSye,dtTableSyrly,"FieldName","Y")
    ''' </summary>
    ''' <param name="dg">DataGrid</param>
    ''' <param name="dtSye">DataTable:DataSource</param>
    ''' <param name="dtTableStyle">DataGridTableStyle DataGrid</param>
    ''' <param name="ColumnA">Column Name:"ColumnA"</param>
    ''' <param name="ColumnB">Column Name:"ColumnB"</param>
    ''' <param name="Amount">���͹�㹡�õ�Ǩ�ͺ ����� True:��Ǩ�ͺ��ҡѹ�������¹��, ����� False:��Ǩ�ͺ�����ҡѹ�������¹��</param>
    ''' <remarks>������¡�� Ex:DataGridEnableEventArgs.addGridStyle(dg,dtSye,dtTableSyrly,"ColumnA","ColumnB",False</remarks>
    Public Shared Sub addGridStyle(ByRef dg As DataGrid, ByVal dtSye As Data.DataTable, ByVal dtTableStyle As System.Windows.Forms.DataGridTableStyle, _
    ByVal ColumnA As String, ByVal ColumnB As String, ByVal Amount As Boolean)
        Try
            If dtSye.Columns.Count <= 0 Then Exit Sub
            If dtTableStyle.GridColumnStyles.Count <= 0 Then Exit Sub
            IsAmount = Amount
            fieldCon = ColumnA
            valuefield = ColumnB
            dtCon = dtSye
            Dim dtStyle As New DataGridTableStyle()
            dtStyle.MappingName = dtSye.TableName
            Dim dgColumn(dtSye.Columns.Count) As ColumnStyle
            For i As Integer = 0 To dtTableStyle.GridColumnStyles.Count - 1

                dgColumn(i) = New ColumnStyle(i)
                'dgColumn(i).Format = ""
                dgColumn(i).HeaderText = dtTableStyle.GridColumnStyles.Item(i).HeaderText
                dgColumn(i).MappingName = dtTableStyle.GridColumnStyles.Item(i).MappingName
                dgColumn(i).Width = dtTableStyle.GridColumnStyles.Item(i).Width
                dgColumn(i).Site = dtTableStyle.GridColumnStyles.Item(i).Site
                dgColumn(i).PropertyDescriptor = dtTableStyle.GridColumnStyles.Item(i).PropertyDescriptor
                AddHandler dgColumn(i).CheckCellEven, AddressOf myStyle_CheckCellContainsAmount
                dtStyle.GridColumnStyles.Add(dgColumn(i))
            Next

            dtStyle.MappingName = dtSye.TableName

            dg.TableStyles.Clear()
            dg.TableStyles.Add(dtStyle)
        Catch ex As Exception
            Throw ex
        End Try

    End Sub
End Class
''' <summary>
''' ���� ��˹� Column Style ���Ѻ DataGrid
''' </summary>
''' <remarks></remarks>
Public Class ColumnStyle
    Inherits DataGridTextBoxColumn
    Public Event CheckCellEven As CheckCellEventHandler

    Private _col As Integer
    ''' <summary>
    ''' ���͡ Column ���С�˹���
    ''' </summary>
    ''' <param name="column">colum Index</param>
    ''' <remarks></remarks>
    Public Sub New(ByVal column As Integer)
        _col = column
    End Sub

    Protected Overloads Overrides Sub Paint(ByVal g As Graphics, ByVal Bounds As Rectangle, ByVal Source As CurrencyManager, ByVal RowNum As Integer, ByVal BackBrush As Brush, ByVal ForeBrush As Brush, ByVal AlignToRight As Boolean)
        Try
            Dim e As New DataGridEnableEventArgs(RowNum, True)
            RaiseEvent CheckCellEven(Me, e)
            If e.MeetsCriteria Then
                '����¹�վ����ѧ������ӹ˹�
                BackBrush = New SolidBrush(cColor.Color)
                'ForeBrush = New SolidBrush(Color.Blue)
            End If
            MyBase.Paint(g, Bounds, Source, RowNum, BackBrush, ForeBrush, AlignToRight)
        Catch

        End Try

        'End If
    End Sub
End Class
''' <summary>
''' ���� ��˹���
''' </summary>
''' <remarks></remarks>
Public Class cColor
    Public Shared Color As System.Drawing.Color = Drawing.Color.Blue
End Class
