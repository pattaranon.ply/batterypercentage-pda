
Public Class Sound
    Public Shared Sub PlayError()
        Try
            Dim filePath As String = System.IO.Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().GetName().CodeBase) & "\error.wav"
            Dim sp As New OpenNETCF.Media.SoundPlayer()
            sp.SoundLocation = filePath
            sp.Play()
        Catch ex As Exception
            Throw
        End Try
    End Sub
End Class
