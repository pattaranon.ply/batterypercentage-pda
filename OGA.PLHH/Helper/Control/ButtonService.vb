Public Class ButtonService


    Private Const BS_MULTILINE As Integer = &H2000
    Private Const GWL_STYLE As Integer = -16

    <System.Runtime.InteropServices.DllImport("coredll")> _
    Private Shared Function GetWindowLong(ByVal hWnd As IntPtr, ByVal nIndex As Integer) As Integer
    End Function

    <System.Runtime.InteropServices.DllImport("coredll")> _
    Private Shared Function SetWindowLong(ByVal hWnd As IntPtr, ByVal nIndex As Integer, ByVal dwNewLong As Integer) As Integer
    End Function

    Public Shared Sub MakeButtonMultiline(ByVal b As Button)
        Dim hwnd As IntPtr = b.Handle
        Dim currentStyle As Integer = GetWindowLong(hwnd, GWL_STYLE)
        Dim newStyle As Integer = SetWindowLong(hwnd, GWL_STYLE, currentStyle Or BS_MULTILINE)
    End Sub

End Class
