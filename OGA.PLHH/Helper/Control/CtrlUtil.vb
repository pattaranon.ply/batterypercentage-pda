Public Class CtrlUtil


    Public Shared Sub SelectTextBox(ByVal targetTextBox As TextBox)
        targetTextBox.Focus()
        ' targetTextBox.Select( targetTextBox.Text.Trim.Length, 1)
        targetTextBox.Select(0, targetTextBox.Text.Trim.Length)
    End Sub

    Public Shared Function checkEnterData(ByVal txtTarget As TextBox, Optional ByVal msg As String = "") As Boolean

        If txtTarget.Text.Trim = String.Empty Then
            If msg = "" Then
                OGAMsg.ShowValidate("��س��������� !")
            Else
                OGAMsg.ShowValidate(msg)
            End If

            txtTarget.Text = String.Empty
            txtTarget.Focus()
            Return False
        Else
            Return True
        End If
    End Function

    Public Shared Function checkSelectData(ByVal cboTarget As ComboBox) As Boolean

        If cboTarget.SelectedIndex = -1 Then
            OGAMsg.ShowValidate("��س����͡������!")
            cboTarget.Focus()
            Return False
        Else
            Return True
        End If
    End Function

    Public Shared Sub lockCtrl(ByVal TextBox As TextBox, ByVal isLock As Boolean)
        If isLock = True Then
            TextBox.BackColor = Color.Gainsboro
            TextBox.ReadOnly = True
        Else
            TextBox.BackColor = Color.White
            TextBox.ReadOnly = False
        End If
    End Sub

    Public Shared Sub lockCtrl(ByVal ComboBox As ComboBox, ByVal isLock As Boolean)
        If isLock = True Then
            ComboBox.BackColor = Color.Gainsboro
            ComboBox.Enabled = False
        Else
            ComboBox.BackColor = Color.White
            ComboBox.Enabled = True
        End If
    End Sub
End Class
