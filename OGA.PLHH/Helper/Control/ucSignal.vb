Public Class ucSignal
    Dim parentForm As Form

    Sub New()
        InitializeComponent()
    End Sub

    Private Sub ucSignal_ParentChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.ParentChanged
        Try
            parentForm = DirectCast(Me.Parent, Form)
            RemoveHandler parentForm.Activated, AddressOf formParent_Activated
            AddHandler parentForm.Activated, AddressOf formParent_Activated
        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try
    End Sub

    Private Sub formParent_Activated(ByVal sender As System.Object, ByVal e As System.EventArgs)
        'Me.GetSignalStrength()
    End Sub

    Private Sub picCheckWifi_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles picCheckWifi.Click
        System.Windows.Forms.MessageBox.Show(Me.GetSignalStrength())
    End Sub

    Private Sub picCheckWifi_DoubleClick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles picCheckWifi.DoubleClick
        System.Windows.Forms.MessageBox.Show(Me.GetSignalStrength())
    End Sub

    Private Function GetSignalStrength() As String
        Dim strength As String = "Can not connect wireless signal"
        Try
            Cursor.Current = Cursors.WaitCursor
            Dim wsOther As WSOther.OtherService
            wsOther = InstanceService.GetWSOtherInstance
            wsOther.HelloWorld()
        Catch exNew As System.Net.WebException
            ' strength = "�������ö����������"
            strength = "Can not connect wireless signal"
            Me.picCheckWifi.Image = My.Resources.SignalLV0
            Return strength
        Catch ex As Exception
            strength = ex.Message
            Me.picCheckWifi.Image = My.Resources.SignalLV0
            Return strength
        Finally
            Cursor.Current = Cursors.Default
        End Try

        Try
            For Each ap As OpenNETCF.Net.Adapter In OpenNETCF.Net.Networking.GetAdapters
                If ap.IsWireless Then
                    Select Case ap.SignalStrength.Strength
                        Case OpenNETCF.Net.StrengthType.NotAWirelessAdapter
                            ' strength = "�������ö����������"
                            strength = "Can not connect wireless signal"
                            Me.picCheckWifi.Image = My.Resources.SignalLV0
                        Case OpenNETCF.Net.StrengthType.NoSignal
                            'strength = "�������ö����������"
                            strength = "No wireless signal"
                            Me.picCheckWifi.Image = My.Resources.SignalLV0
                        Case OpenNETCF.Net.StrengthType.VeryLow
                            ' strength = "�ѭ�ҳ����ҡ"
                            strength = "Very low signal"
                            Me.picCheckWifi.Image = My.Resources.SignalLV1
                        Case OpenNETCF.Net.StrengthType.Low
                            ' strength = "�ѭ�ҳ���"
                            strength = "Low signal"
                            Me.picCheckWifi.Image = My.Resources.SignalLV2
                        Case OpenNETCF.Net.StrengthType.Good
                            'strength = "�ѭ�ҳ����"
                            strength = "Good signal"
                            Me.picCheckWifi.Image = My.Resources.SignalLV3
                        Case OpenNETCF.Net.StrengthType.VeryGood
                            'strength = "�ѭ�ҳ��"
                            strength = "Very Good signal"
                            Me.picCheckWifi.Image = My.Resources.SignalLV4
                        Case OpenNETCF.Net.StrengthType.Excellent
                            'strength = "�ѭ�ҳ���ҡ"
                            strength = "Excellent signal"
                            Me.picCheckWifi.Image = My.Resources.SignalLV5
                        Case Else
                            ' strength = "�������ö����������"
                            strength = "No signal"
                            Me.picCheckWifi.Image = My.Resources.SignalLV0
                    End Select

                    picCheckWifi.Refresh()
                End If
            Next
        Catch ex As Exception
            strength = "�������ö����������"
            Me.picCheckWifi.Image = My.Resources.SignalLV0
        End Try

        Return strength
    End Function


End Class
