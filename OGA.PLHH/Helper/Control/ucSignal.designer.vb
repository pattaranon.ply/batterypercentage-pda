<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Public Class ucSignal
    Inherits System.Windows.Forms.UserControl

    'UserControl overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(ucSignal))
        Me.picCheckWifi = New System.Windows.Forms.PictureBox
        Me.SuspendLayout()
        '
        'picCheckWifi
        '
        Me.picCheckWifi.BackColor = System.Drawing.Color.FromArgb(CType(CType(192, Byte), Integer), CType(CType(192, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.picCheckWifi.Dock = System.Windows.Forms.DockStyle.Top
        Me.picCheckWifi.Image = CType(resources.GetObject("picCheckWifi.Image"), System.Drawing.Image)
        Me.picCheckWifi.Location = New System.Drawing.Point(0, 0)
        Me.picCheckWifi.Name = "picCheckWifi"
        Me.picCheckWifi.Size = New System.Drawing.Size(40, 20)
        Me.picCheckWifi.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        '
        'ucSignal
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(96.0!, 96.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi
        Me.BackColor = System.Drawing.Color.FromArgb(CType(CType(192, Byte), Integer), CType(CType(192, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.Controls.Add(Me.picCheckWifi)
        Me.Name = "ucSignal"
        Me.Size = New System.Drawing.Size(40, 20)
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents picCheckWifi As System.Windows.Forms.PictureBox

End Class
