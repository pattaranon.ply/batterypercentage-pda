﻿'Imports System.Linq
Imports System.Collections.Generic
Imports System.Text
Imports System.Runtime.InteropServices

Public Class APISetTime

    <DllImport("coredll.dll", EntryPoint:="SetTimeZoneInformation", SetLastError:=True)> _
    Public Shared Function SetTimeZoneInformation(ByRef TZI As TIME_ZONE_INFORMATION) As UInt32
    End Function

    <DllImport("coredll.dll", EntryPoint:="SetSystemTime", SetLastError:=True)> _
    Public Shared Function WinCESetSystemTime(ByRef st As SystemTime) As Boolean
    End Function

    <DllImport("coredll.dll", CharSet:=CharSet.Auto)> _
    Private Shared Function GetTimeZoneInformation(ByRef lpTimeZoneInformation As TIME_ZONE_INFORMATION) As Integer
    End Function

    <DllImport("coredll.dll", SetLastError:=True)> _
    Private Shared Function SetLocalTime(ByRef st As SystemTime) As Boolean
    End Function

    <DllImport("coredll.dll", SetLastError:=True)> _
    Private Shared Function SystemTimeToTzSpecificLocalTime(ByVal lpTimeZoneInformation As IntPtr, ByRef lpUniversalTime As SystemTime, ByRef lpLocalTime As SystemTime) As Long
    End Function

    Public Structure TIME_ZONE_INFORMATION
        Public Bias As Integer

        <MarshalAs(UnmanagedType.ByValTStr, SizeConst:=32)> _
        Public StandardName As String

        Public StandardDate As SystemTime

        Public StandardBias As Integer

        <MarshalAs(UnmanagedType.ByValTStr, SizeConst:=32)> _
        Public DaylightName As String

        Public DaylightDate As SystemTime

        Public DaylightBias As Integer
    End Structure

    Public Structure SystemTime
        Public Year As UShort

        Public Month As UShort

        Public DayOfWeek As UShort

        Public Day As UShort

        Public Hour As UShort

        Public Minute As UShort

        Public Second As UShort

        Public Millisecond As UShort

    End Structure

    Public Sub UpdateSystemTime(ByVal utc As DateTime)
        Dim tzI As New TIME_ZONE_INFORMATION()
        'tzI.Bias = 0;
        Dim r As UInteger
        'r = SetTimeZoneInformation(ref tzI);
        'tzI.Bias = -420;//-7 * 60;//GMT-8
        'tzI.StandardName = "Pacific Standard Time";
        'tzI.DaylightName = "Pacific Daylight Time";
        'tzI.DaylightBias = -60;
        r = SetTimeZoneInformation(tzI)

        Dim st As New SystemTime()
        st.Year = CUShort(utc.Year)
        st.Month = CUShort(utc.Month)
        st.Day = CUShort(utc.Day)
        st.DayOfWeek = CUShort(utc.DayOfWeek)
        st.Hour = CUShort((utc.Hour) Mod 24)
        st.Minute = CUShort(utc.Minute)
        st.Second = 30
        st.Millisecond = CUShort(utc.Millisecond)


        If r = 0 Then
            System.Diagnostics.Debug.WriteLine(Marshal.GetLastWin32Error().ToString())
        End If


        Dim result As Boolean = WinCESetSystemTime(st)
        'bool result = SetLocalTime(ref st);if (true != result)
        If True Then
            Dim [error] As Integer = Marshal.GetLastWin32Error()
            'Debug.WriteLine(String.Format("SetSystemTime failed: {0:X8}", error));
        End If
    End Sub

    Public Sub SetTimeZone(ByVal timeZone As Int32)
        Dim tzI As New TIME_ZONE_INFORMATION()
        tzI.Bias = timeZone
        SetTimeZoneInformation(tzI)
        'tzI.Bias = -420;//-7 * 60;//GMT-8

    End Sub

    Public Sub SetTime(ByVal utc As DateTime)
        Dim st As New SystemTime()
        st.Year = CUShort(utc.Year)
        st.Month = CUShort(utc.Month)
        st.Day = CUShort(utc.Day)
        st.DayOfWeek = CUShort(utc.DayOfWeek)
        st.Hour = CUShort((utc.Hour) Mod 24)
        st.Minute = CUShort(utc.Minute)
        st.Second = CUShort(utc.Second)
        st.Millisecond = CUShort(utc.Millisecond)
        WinCESetSystemTime(st)
    End Sub

End Class
