﻿Imports System.Reflection
Imports System.Resources

Public Class WMSMsg

    Public Shared Sub ShowAsterisk(ByVal msg As String)
        MessageBox.Show(msg, "ผลการตรวจสอบ", MessageBoxButtons.OK, MessageBoxIcon.Asterisk, MessageBoxDefaultButton.Button1)
    End Sub

    Public Shared Sub ShowPleaseSpecifyData(ByVal msg As String)
        MessageBox.Show(String.Format("กรุณาระบุข้อมูล {0} !", msg), "ผลการตรวจสอบ", MessageBoxButtons.OK, MessageBoxIcon.Asterisk, MessageBoxDefaultButton.Button1)
    End Sub

    Public Shared Sub ShowResult(ByVal msg As String)
        MessageBox.Show(msg, "ผลการทำงาน", MessageBoxButtons.OK, MessageBoxIcon.Asterisk, MessageBoxDefaultButton.Button1)
    End Sub

    Public Shared Sub ShowValidate(ByVal msg As String)
        MessageBox.Show(msg, "ผลการตรวจสอบ", MessageBoxButtons.OK, MessageBoxIcon.Asterisk, MessageBoxDefaultButton.Button1)
    End Sub


    Public Shared Sub ShowInformation(ByVal msg As String)
        MessageBox.Show(msg, "ผลการตรวจสอบ", MessageBoxButtons.OK, MessageBoxIcon.Asterisk, MessageBoxDefaultButton.Button1)
    End Sub

    Public Shared Sub ShowErrorTryCatchBarcode(ByVal ex As Exception)
        MessageBox.Show(ex.Message, "ข้อผิดพลาด", MessageBoxButtons.OK, MessageBoxIcon.Exclamation, MessageBoxDefaultButton.Button1)
    End Sub

    Public Shared Sub ShowErrorTryCatch(ByVal ex As Exception)
        MessageBox.Show("ข้อผิดพลาดจากระบบ : " & ex.ToString, "ข้อผิดพลาด", MessageBoxButtons.OK, MessageBoxIcon.Exclamation, MessageBoxDefaultButton.Button1)
    End Sub

    Public Shared Sub ShowErrorTryCatch(ByVal msg As String, ByVal ex As Exception)
        MessageBox.Show(msg & vbCrLf & "ข้อผิดพลาดจากระบบ : " & ex.ToString, "ข้อผิดพลาด", MessageBoxButtons.OK, MessageBoxIcon.Exclamation, MessageBoxDefaultButton.Button1)
    End Sub

    Public Shared Sub ShowError(ByVal msg As String)
        MessageBox.Show("ข้อผิดพลาดจากระบบ : " & msg, "ข้อผิดพลาด", MessageBoxButtons.OK, MessageBoxIcon.Exclamation, MessageBoxDefaultButton.Button1)
    End Sub

    Public Shared Sub ShowErrorBarcode(ByVal msg As String)
        MessageBox.Show(msg, "ข้อผิดพลาด", MessageBoxButtons.OK, MessageBoxIcon.Exclamation, MessageBoxDefaultButton.Button1)
    End Sub

    Public Shared Sub ShowErrorFromWS(ByVal msg As String)
        MessageBox.Show(msg & ControlChars.NewLine & "(การตรวจสอบจาก Server)", "ผลการทำงาน", MessageBoxButtons.OK, MessageBoxIcon.Asterisk, MessageBoxDefaultButton.Button1)
    End Sub

    Public Shared Sub ShowErrorConnectWebService(ByVal ex As System.Net.WebException)
        MessageBox.Show("ไม่สามารถติดต่อ Server ได้ !" & vbCrLf & "อาจไม่มีคลื่นสัญญาน" & vbCrLf & "กรุณาลองอีกครั้ง !", "ผลการทำงาน", MessageBoxButtons.OK, MessageBoxIcon.Exclamation, MessageBoxDefaultButton.Button1)
    End Sub

    Public Shared Function ShowConfirm(ByVal msg As String) As Windows.Forms.DialogResult
        Return MessageBox.Show(msg, "กรุณายืนยัน", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2)
    End Function


    Public Shared Function ShowConfirmDelete(ByVal data As String) As Windows.Forms.DialogResult
        Return MessageBox.Show("คุณต้องการลบข้อมูล : " & data & " ใช่ หรือ ไม่ ?", "กรุณายืนยัน", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2)
    End Function


    Public Shared Function ShowSaveCompleted() As Windows.Forms.DialogResult
        Return MessageBox.Show("บันทึกข้อมูล เสร็จสมบูรณ์ !", "ผลการทำงาน", MessageBoxButtons.OK, MessageBoxIcon.Asterisk, MessageBoxDefaultButton.Button1)
    End Function


End Class
