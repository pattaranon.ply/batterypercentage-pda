Imports System.Reflection
Imports System.Resources

Public Class OGAMsg

    Public Shared Sub ShowAsterisk(ByVal msg As String)
        MessageBox.Show(msg, "Validating", MessageBoxButtons.OK, MessageBoxIcon.Asterisk, MessageBoxDefaultButton.Button1)
    End Sub

    Public Shared Sub ShowPleaseSpecifyData(ByVal msg As String)
        MessageBox.Show(String.Format("��س��кآ����� {0} !", msg), "�š�õ�Ǩ�ͺ", MessageBoxButtons.OK, MessageBoxIcon.Asterisk, MessageBoxDefaultButton.Button1)
    End Sub

    Public Shared Sub ShowResult(ByVal msg As String)
        MessageBox.Show(msg, "Result", MessageBoxButtons.OK, MessageBoxIcon.Asterisk, MessageBoxDefaultButton.Button1)
    End Sub

    Public Shared Sub ShowValidate(ByVal msg As String)
        MessageBox.Show(msg, "Validating", MessageBoxButtons.OK, MessageBoxIcon.Asterisk, MessageBoxDefaultButton.Button1)
    End Sub


    Public Shared Sub ShowInformation(ByVal msg As String)
        MessageBox.Show(msg, "Information", MessageBoxButtons.OK, MessageBoxIcon.Asterisk, MessageBoxDefaultButton.Button1)
    End Sub

    Public Shared Sub ShowErrorTryCatchBarcode(ByVal ex As Exception)
        MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Exclamation, MessageBoxDefaultButton.Button1)
    End Sub

    Public Shared Sub ShowErrorTryCatch(ByVal ex As Exception)
        MessageBox.Show("System error : " & ex.ToString, "Error", MessageBoxButtons.OK, MessageBoxIcon.Exclamation, MessageBoxDefaultButton.Button1)
    End Sub

    Public Shared Sub ShowErrorTryCatch(ByVal msg As String, ByVal ex As Exception)
        MessageBox.Show(msg & vbCrLf & "System error : " & ex.ToString, "Error", MessageBoxButtons.OK, MessageBoxIcon.Exclamation, MessageBoxDefaultButton.Button1)
    End Sub

    Public Shared Sub ShowError(ByVal msg As String)
        MessageBox.Show("Error : " & vbCrLf & msg, "Error", MessageBoxButtons.OK, MessageBoxIcon.Exclamation, MessageBoxDefaultButton.Button1)
    End Sub

    Public Shared Sub ShowErrorBarcode(ByVal msg As String)
        MessageBox.Show(msg, "Error", MessageBoxButtons.OK, MessageBoxIcon.Exclamation, MessageBoxDefaultButton.Button1)
    End Sub

    Public Shared Sub ShowErrorFromWS(ByVal msg As String)
        MessageBox.Show(msg & ControlChars.NewLine & "(Result from Server)", "Error from Server", MessageBoxButtons.OK, MessageBoxIcon.Exclamation, MessageBoxDefaultButton.Button1)
    End Sub

    Public Shared Sub ShowErrorConnectWebService(ByVal ex As System.Net.WebException)
        MessageBox.Show("Can not connect server" & vbCrLf & "because no wireless signal" & vbCrLf & "Please try again !", "Result", MessageBoxButtons.OK, MessageBoxIcon.Exclamation, MessageBoxDefaultButton.Button1)
    End Sub

    Public Shared Function ShowConfirm(ByVal msg As String) As Windows.Forms.DialogResult
        Return MessageBox.Show(msg, "Confirm", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2)
    End Function

End Class
