Public Class BarcodetInspect

#Region "Fields"

#End Region

#Region "Constructors"

#End Region

#Region "Properties"

#End Region

#Region "Methods"
    ''' <summary>
    ''' ��ҹ��е�Ǩ�ͺ�������Թ���
    ''' </summary>
    ''' <param name="BarcodeData"></param>
    ''' <returns></returns>
    ''' <remarks>Exception : BarcodeInspectException</remarks>
    Public Shared Function InspectBarcodeRack(ByVal BarcodeData As String) As BarcodeRackDetail
        Dim Result As BarcodeRackDetail = New BarcodeRackDetail()

        BarcodeData = BarcodeData.Trim.ToUpper()
        Try
            If BarcodeData.Length <> 6 Then
                Throw New BarcodeInspectException("Wrong format barcode !" & vbCrLf & "Length <> 6 " & vbCrLf & "Please try again !")
            End If

            If BarcodeData.Substring(0, 1) <> "S" Then
                Throw New BarcodeInspectException("Wrong format barcode !" & vbCrLf & "Prefix not 'S'" & vbCrLf & "Please try again !")
            End If

            If IsNumeric(BarcodeData.Substring(1, 5)) = False Then
                Throw New BarcodeInspectException("Wrong format barcode !" & vbCrLf & "Running No not numeric" & vbCrLf & "Please try again !")
            End If

            Result.Barcode = BarcodeData

            'If chkFormat.Length <> 2 Then
            '    Throw New BarcodeInspectException(String.Empty)
            'Else
            '    Result.Barcode = BarcodeData
            '    Result.MaterialCode = chkFormat(0).ToUpper.Trim
            '    Result.Lot = chkFormat(1).ToUpper.Trim
            'End If

        Catch ex As Exception
            Throw
        End Try
        Return Result
    End Function

#End Region
End Class

''' <summary>
''' Exception ����Դ�ҡ���� BarcodeInspect
''' </summary>
''' <remarks></remarks>
Public Class BarcodeInspectException
    Inherits Exception

    Sub New(ByVal ErrMsg As String)
        MyBase.New(ErrMsg)
    End Sub

    Public Overrides ReadOnly Property Message() As String
        Get
            Return MyBase.Message
        End Get
    End Property
End Class
