Public Class BarcodeRackDetail

#Region "Fields"
    'Private _MaterialCode As String
    'Private _Lot As String
    Private _Barcode As String
    'Private _ProductionDate As String

#End Region

#Region "Constructors"

#End Region

#Region "Properties"

    'Public Property MaterialCode() As String
    '    Get
    '        Return _MaterialCode
    '    End Get
    '    Set(ByVal value As String)
    '        _MaterialCode = value
    '    End Set
    'End Property


    'Public Property Lot() As String
    '    Get
    '        Return _Lot
    '    End Get
    '    Set(ByVal value As String)
    '        _Lot = value
    '    End Set
    'End Property

    Public Property Barcode() As String
        Get
            Return _Barcode
        End Get
        Set(ByVal value As String)
            _Barcode = value
        End Set
    End Property

    'Public Property ProductionDate() As String
    '    Get
    '        Return _ProductionDate
    '    End Get
    '    Set(ByVal value As String)
    '        _ProductionDate = value
    '    End Set
    'End Property


#End Region

#Region "Methods"

#End Region

End Class

