Imports System.Data

Public Class frmGRScan

    Private Sub frmGRScan_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Me.showAllRackID()
        Me.initCtrl()
    End Sub

    Private Sub initCtrl()
        Me.txtScan.Text = String.Empty
        Me.txtScan.Focus()
    End Sub

#Region "showAllRackID"
    Private Sub showAllRackID()
        Try
            Cursor.Current = Cursors.WaitCursor
            Dim dal As New GRDAL
            GlobalVariable.dsGR = dal.getAllData

            If GlobalVariable.dsGR.Tables(0).Rows.Count <> 0 Then
                Me.dgDetail.DataSource = GlobalVariable.dsGR.Tables("tbt_GRScan")
                Me.lblTotalQty.Text = "Total Qty : " & GlobalVariable.dsGR.Tables(0).Rows.Count
            Else
                Me.dgDetail.DataSource = Nothing
                Me.lblTotalQty.Text = "Total Qty : 0"
            End If
        Catch ex As Exception
            OGAMsg.ShowErrorTryCatch("Get Pallet data error !", ex)
        Finally
            Cursor.Current = Cursors.Default
        End Try
    End Sub
#End Region

    Private Sub txtScan_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txtScan.KeyPress
        If e.KeyChar = Chr(Keys.Enter) Then
            If CtrlUtil.checkEnterData(Me.txtScan) = False Then
                Exit Sub
            End If
            Me.InspectBarcode()
        End If
    End Sub

#Region "Insert Data"
    Dim barcode As New BarcodeRackDetail()

    Private Sub InspectBarcode()
        Try
            Cursor.Current = Cursors.WaitCursor

            Me.barcode = BarcodetInspect.InspectBarcodeRack(Me.txtScan.Text.Trim)
            Dim grEnt As New GREntity
            grEnt.RackID = Me.barcode.Barcode
            grEnt.ScanDatetime = Date.Now
            grEnt.ScanDatetimeShow = Format(grEnt.ScanDatetime, "dd/MM/yyyy HH:mm:ss")


            'Check scan barcode rack is duplicate before add to dataset ----------------------
            If GlobalVariable.dsGR.Tables(0).Rows.Count <> 0 Then
                For i As Integer = 0 To GlobalVariable.dsGR.Tables(0).Rows.Count - 1
                    If grEnt.RackID = GlobalVariable.dsGR.Tables(0).Rows(i).Item("RackID").ToString.Trim Then
                        Sound.PlayError()
                        OGAMsg.ShowValidate("Scan Pallet " & vbCrLf & "ID : " & grEnt.RackID & vbCrLf & "is duplicate !" & vbCrLf & "Please try again !")
                        Me.initCtrl()
                        Exit Sub
                    End If
                Next i
            End If
            '---------------------------------------------------------------------------------

            Dim dal As New GRDAL

            If dal.insertData(grEnt) = True Then
                Me.showAllRackID()
                Me.initCtrl()
            End If

        Catch exBarcode As BarcodeInspectException
            OGAMsg.ShowErrorTryCatchBarcode(exBarcode)
            Me.initCtrl()
        Catch ex As Exception
            OGAMsg.ShowError(ex.ToString)
            Me.initCtrl()
        Finally
            Cursor.Current = Cursors.Default
        End Try
    End Sub

#End Region

    Private Sub btnBack_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnBack.Click
        If GlobalVariable.ConnectionStatus = "Online" Then
            If GlobalVariable.dsGR.Tables(0).Rows.Count <> 0 Then
                If OGAMsg.ShowConfirm("Do you want to finish work ?") = Windows.Forms.DialogResult.Yes Then
                    Me.confirmDataToServer(True) '
                Else
                    Me.Close()
                End If
            Else
                Me.Close()
            End If
        Else
            'Connection Status = Offline
            'OGAMsg.ShowValidate("Can not finish work !" & vbCrLf & "It is Offline mode.")
            Me.Close()
        End If

       

    End Sub

    Private Sub btnSendToServer_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSendToServer.Click
        If GlobalVariable.ConnectionStatus = "Online" Then
            If GlobalVariable.dsGR.Tables(0).Rows.Count = 0 Then
                OGAMsg.ShowValidate("No data for finish work !")
                Me.initCtrl()
                Exit Sub
            End If

            If OGAMsg.ShowConfirm("Are you sure to finish work !") = Windows.Forms.DialogResult.Yes Then
                Me.confirmDataToServer()
            End If
        Else
            'Connection Status = Offline
            OGAMsg.ShowValidate("Can not finish work !" & vbCrLf & "It is Offline mode.")
        End If
       
    End Sub

#Region "confirmDataToServer"
    Private Sub confirmDataToServer(Optional ByVal isClose As Boolean = False)
        Try
            Cursor.Current = Cursors.WaitCursor
            Dim wsGR As WSGR.GRService
            wsGR = InstanceService.GetWSGRInstance

            Dim requestEnt As New WSGR.GREntity
            requestEnt.ListConfirmData = GlobalVariable.dsGR

            Dim resultEnt As New WSGR.GREntity

            resultEnt = wsGR.WriteScanDataToServer(requestEnt)

            If resultEnt.IsComplete = True Then
                Dim dal As New GRDAL
                dal.deleteAllData()
                Me.showAllRackID()
                OGAMsg.ShowResult("Finish work completed !")
                Me.initCtrl()
                If isClose = True Then
                    Me.Close()
                End If
            Else
                OGAMsg.ShowErrorFromWS(resultEnt.ErrorMessage)
                Me.initCtrl()
            End If

        Catch ex As System.Net.WebException
            OGAMsg.ShowErrorConnectWebService(ex)
        Catch ex As Exception
            OGAMsg.ShowErrorTryCatch(ex)
        Finally
            Cursor.Current = Cursors.Default
        End Try
    End Sub
#End Region


    Private Sub btnDelete_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnDelete.Click
        Me.DeleteScanData()
    End Sub

#Region "DeleteScanData"
    Private Sub DeleteScanData()
        Try

            If GlobalVariable.dsGR.Tables(0).Rows.Count = 0 Then
                OGAMsg.ShowValidate("No data for delete !")
                Exit Sub
            End If


            Cursor.Current = Cursors.WaitCursor

            Dim dr As DataRow = GlobalVariable.dsGR.Tables(0).Rows(Me.dgDetail.CurrentRowIndex)
            Dim rackID As String
            rackID = dr("RackID").ToString.Trim



            If OGAMsg.ShowConfirm("Are you sure to delete" & _
                      vbCrLf & "Pallet ID : " & rackID & _
                      vbCrLf & "Yes or No ?") <> Windows.Forms.DialogResult.Yes Then
                Exit Sub
            End If

            Cursor.Current = Cursors.WaitCursor
            GlobalVariable.dsGR.Tables(0).Rows.Remove(dr)
            GlobalVariable.dsGR.Tables(0).AcceptChanges()
            Dim dal As New GRDAL
            dal.writeForDelete()

            Me.showAllRackID()
            OGAMsg.ShowResult("Delete Pallet ID : " & rackID & vbCrLf & "completed !")
            Me.initCtrl()

        Catch ex As Exception
            OGAMsg.ShowErrorTryCatch("Delete Pallet error !" & vbCrLf & "Error !", ex)
        Finally
            Cursor.Current = Cursors.Default
        End Try
    End Sub
#End Region
End Class









'Private Sub frmGRScan_KeyDown(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles MyBase.KeyDown
'    If (e.KeyCode = System.Windows.Forms.Keys.Up) Then
'        'Up
'    End If
'    If (e.KeyCode = System.Windows.Forms.Keys.Down) Then
'        'Down
'    End If
'    If (e.KeyCode = System.Windows.Forms.Keys.Left) Then
'        'Left
'    End If
'    If (e.KeyCode = System.Windows.Forms.Keys.Right) Then
'        'Right
'    End If
'    If (e.KeyCode = System.Windows.Forms.Keys.Enter) Then
'        'Enter
'    End If

'End Sub