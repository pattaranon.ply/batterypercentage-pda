Imports System.Xml
Imports System.Data
Imports System.IO


Public Class frmGRDocSummary

    Public isUpdate As Boolean = False

    Private Sub frmGRDocSummary_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Me.loadXmlPO()
        Me.loadXmlScanGR()
        Me.showGridDetail()
    End Sub

    Private Sub btnScan_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnScan.Click
        Dim f As New frmGRScanReceive
        f.ShowDialog()
        If f.isUpdate = True Then
            Me.loadXmlPO()
            Me.loadXmlScanGR()
            Me.showGridDetail()
        End If
    End Sub

    Private Sub btnViewDetail_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnViewDetail.Click
        Dim f As New frmGRViewDetail
        f.txtSection.Text = Me.dgvDetail.Item(Me.dgvDetail.CurrentCell.RowNumber, 0).ToString()
        f.txtOrder.Text = Me.dgvDetail.Item(Me.dgvDetail.CurrentCell.RowNumber, 1).ToString()
        f.orderSeq = Me.dgvDetail.Item(Me.dgvDetail.CurrentCell.RowNumber, 4).ToString()
        f.ShowDialog()
        If f.isUpdate = True Then
            Me.isUpdate = True
            Me.writeXML()
            Me.loadXmlPO()
            Me.loadXmlScanGR()
            Me.showGridDetail()
        End If
    End Sub

    Private Sub writeXML()
        Try
            If Me.isUpdate = True Then
                If GlobalVariable.dsGR.Tables(0).Rows.Count = 0 Then
                    GlobalVariable.dsGR.WriteXml(GlobalVariable.POXmlPath, XmlWriteMode.WriteSchema)
                    GlobalVariable.dsScanGR.WriteXml(GlobalVariable.POScanXmlPath, XmlWriteMode.WriteSchema)
                Else
                    GlobalVariable.dsGR.WriteXml(GlobalVariable.POXmlPath)
                    GlobalVariable.dsScanGR.WriteXml(GlobalVariable.POScanXmlPath)
                End If
            End If
        Catch ex As Exception
            WMSMsg.ShowErrorTryCatch("�����¹������ŧ XML ��� �Դ��Ҵ !", ex)
        End Try
    End Sub


    Private Sub btnExit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnExit.Click
        Me.DialogResult = Windows.Forms.DialogResult.No
    End Sub

    Private Sub loadXmlPO()
        Try
            Cursor.Current = Cursors.WaitCursor

            If File.Exists(GlobalVariable.POXmlPath) = False Then
                WMSMsg.ShowValidate("��辺��� XML ������" & vbCrLf & "�ҹ�Ѻ�Թ��� !" & vbCrLf & "��س� Download" & vbCrLf & "�ҡ PC ��͹")
                Me.DialogResult = Windows.Forms.DialogResult.Cancel
                Exit Sub
            End If

            GlobalVariable.dsGR = New DataSet
            GlobalVariable.dsGR.ReadXml(GlobalVariable.POXmlPath)

            Dim dtTmp As DataTable
            dtTmp = GlobalVariable.dsGR.Tables(0).Clone
            dtTmp.Columns("PlanQty").DataType = GetType(Integer)
            dtTmp.Columns("CheckQty").DataType = GetType(Integer)

            For Each dr As DataRow In GlobalVariable.dsGR.Tables(0).Rows
                dtTmp.ImportRow(dr)
            Next

            GlobalVariable.dsGR.Tables.RemoveAt(0)
            GlobalVariable.dsGR.Tables.Add(dtTmp.Copy)

        Catch ex As Exception
            WMSMsg.ShowErrorTryCatch("�����Ŵ�������͡���" & vbCrLf & "����Ѻ �Դ��Ҵ !", ex)
        Finally
            Cursor.Current = Cursors.Default
        End Try
    End Sub

    Private Sub loadXmlScanGR()
        Try
            Cursor.Current = Cursors.WaitCursor

            If File.Exists(GlobalVariable.POScanXmlPath) = False Then
                WMSMsg.ShowValidate("��辺��� XML ������" & vbCrLf & "����᡹��Ǩ�ͺ�Թ���!" & vbCrLf & "��س� Download" & vbCrLf & "�ҡ PC ��͹")
                Exit Sub
            End If

            GlobalVariable.dsScanGR = New DataSet
            GlobalVariable.dsScanGR.ReadXml(GlobalVariable.POScanXmlPath)

        Catch ex As Exception
            WMSMsg.ShowErrorTryCatch("�����Ŵ�����š���᡹" & vbCrLf & "��Ǩ�Ѻ�Թ��� �Դ��Ҵ !", ex)
        Finally
            Cursor.Current = Cursors.Default
        End Try
    End Sub


    Private Sub showGridDetail()
        Try
            Cursor.Current = Cursors.WaitCursor

            Dim dtDistinct As DataTable = GlobalVariable.dsGR.Tables(0).DefaultView.ToTable(True, "Section", "OrderNo", "OrderSeq")


            Dim dtTmp As DataTable
            dtTmp = dtDistinct.Copy

            dtDistinct.Columns.Add(New DataColumn("PlanQty", GetType(Integer)))
            dtDistinct.Columns.Add(New DataColumn("CheckQty", GetType(Integer)))

            dtDistinct.TableName = "TableDetail"

            Dim sumPlanQty, sumCheckQty As Double

            Dim section, orderNo, orderSeq As String

            For i As Integer = 0 To dtTmp.Rows.Count - 1
                section = dtTmp.Rows(i).Item("Section").ToString
                orderNo = dtTmp.Rows(i).Item("OrderNo").ToString
                orderSeq = dtTmp.Rows(i).Item("OrderSeq").ToString

                sumPlanQty = GlobalVariable.dsGR.Tables(0).Compute("SUM(PlanQty)", "Section = '" & section & "' AND OrderNo = '" & orderNo & "' AND OrderSeq = '" & orderSeq & "'")
                sumCheckQty = GlobalVariable.dsGR.Tables(0).Compute("SUM(CheckQty)", "Section = '" & section & "' AND OrderNo = '" & orderNo & "' AND OrderSeq = '" & orderSeq & "'")

                'sumPlanQty = CDbl(GlobalVariable.dsGR.Tables(0).Compute("SUM(PlanQty)", ""))
                'sumCheckQty = CDbl(GlobalVariable.dsGR.Tables(0).Compute("SUM(CheckQty)", ""))

                For j As Integer = 0 To dtDistinct.Rows.Count - 1
                    If dtDistinct.Rows(j).Item("Section").ToString = section And _
                       dtDistinct.Rows(j).Item("OrderNo").ToString = orderNo And _
                       dtDistinct.Rows(j).Item("OrderSeq").ToString = orderSeq Then
                        Dim dr As DataRow
                        dr = dtDistinct.Rows(j)
                        dr.BeginEdit()
                        dr("PlanQty") = CInt(sumPlanQty)
                        dr("CheckQty") = CInt(sumCheckQty)
                        dr.EndEdit()
                        Exit For
                    End If

                Next j

            Next i
            dtDistinct.AcceptChanges()

          
            Me.dgvDetail.DataSource = dtDistinct
            DataGridEnableEventArgs.addGridStyle(Me.dgvDetail, dtDistinct, Me.DataGridTableStyle1, "PlanQty", "CheckQty", False)
        Catch ex As Exception
            WMSMsg.ShowErrorTryCatch("����ʴ�������" & vbCrLf & "��¡���͡��á���Ѻ�Թ���" & vbCrLf & "�Դ��Ҵ !", ex)
        Finally
            Cursor.Current = Cursors.Default
        End Try
    End Sub


End Class