Imports System.Xml
Imports System.Data
Imports System.IO

Public Class frmGRScanReceive

    Public isUpdate As Boolean = False

    Private Sub frmGRScanReceive_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Me.initCtrl()
        Me.initScan()
    End Sub

    Private Sub initScan()
        Me.txtScan.Text = String.Empty
        Me.txtScan.Focus()
    End Sub

    Private Sub initCtrl()
        Me.txtSection.Text = String.Empty
        Me.txtItemName.Text = String.Empty
        Me.txtLocation.Text = String.Empty
        Me.txtScanQty.Text = String.Empty
        Me.txtPlanQty.Text = String.Empty
        Me.txtCheckQty.Text = String.Empty
        Me.txtOrder.Text = String.Empty
    End Sub

    Private Sub btnExit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnExit.Click
        Me.writeXML()
        Me.DialogResult = Windows.Forms.DialogResult.Cancel
    End Sub

    Private Sub writeXML()
        Try
            If Me.isUpdate = True Then
                If GlobalVariable.dsGR.Tables(0).Rows.Count = 0 Then
                    GlobalVariable.dsGR.WriteXml(GlobalVariable.POXmlPath, XmlWriteMode.WriteSchema)
                    GlobalVariable.dsScanGR.WriteXml(GlobalVariable.POScanXmlPath, XmlWriteMode.WriteSchema)
                Else
                    GlobalVariable.dsGR.WriteXml(GlobalVariable.POXmlPath)
                    GlobalVariable.dsScanGR.WriteXml(GlobalVariable.POScanXmlPath)
                End If
            End If
        Catch ex As Exception
            WMSMsg.ShowErrorTryCatch("�����¹������ŧ XML ��� �Դ��Ҵ !", ex)
        End Try
    End Sub

    Private Sub txtScan_KeyPress(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txtScan.KeyPress
        If e.KeyChar = Chr(Keys.Enter) Then
            If CtrlUtil.checkEnterData(Me.txtScan) = False Then
                Exit Sub
            End If
            If Me.checkBarcodeDup(Me.txtScan.Text.Trim) = True Then
                Me.scanBarcode(Me.txtScan.Text.Trim)
            End If
        End If
    End Sub

    Private Function checkBarcodeDup(ByVal barcode As String) As Boolean
        Try
            Me.initCtrl()

            Cursor.Current = Cursors.WaitCursor
            For Each dr As DataRow In GlobalVariable.dsScanGR.Tables(0).Rows
                If dr("OrderSeq").ToString <> "Temp" Then
                    If dr("Barcode").ToString.ToUpper.Trim = barcode.ToUpper.Trim Then
                        Cursor.Current = Cursors.Default
                        WMSMsg.ShowValidate("�س�᡹�����鴫�� !" & vbCrLf & "��سҵ�Ǩ�ͺ�ա����")
                        Me.initScan()
                        Return False
                    End If
                End If
            Next dr
            Return True
        Catch ex As Exception
            WMSMsg.ShowErrorTryCatch("��õ�Ǩ�����鴫�� �Դ��Ҵ !", ex)
        Finally
            Cursor.Current = Cursors.Default
        End Try
    End Function

    Private Sub scanBarcode(ByVal barcode As String)
        Try
            'Me.initCtrl()
            '˹��§ҹ$�����Թ���$�����$�ӹǹ�Թ��ҵ�͡��ͧ$�Ţ��� PO$�Ţ��� SO$�ѹ����Ѻ$UniqueID
            Dim tmp() As String
            tmp = barcode.Split("$")
            If tmp.Length <> 8 Then
                WMSMsg.ShowValidate("�����鴼Դ�ٻẺ !" & vbCrLf & vbCrLf & "��سҵ�Ǩ�ͺ������ : " & barcode)
                Me.initScan()
                Exit Sub
            End If
            Dim requestEnt As New BarcodeEntity
            '˹��§ҹ$�����Թ���$�����$�ӹǹ��͡��ͧ
            '˹��§ҹ$�����Թ���$�����$�ӹǹ��͡��ͧ$�Ţ��� PO$�Ţ��� SO$�ѹ����Ѻ$UniqueID

            requestEnt.Section = tmp(0).ToString.Trim
            requestEnt.ItemCode = tmp(1).ToString.Trim
            requestEnt.Location = tmp(2).ToString.Trim
            requestEnt.PONo = tmp(4).ToUpper.Trim
            requestEnt.SaleOrder = tmp(5).ToUpper.Trim

            If requestEnt.PONo.Trim <> "" Then
                requestEnt.OrderNo = requestEnt.PONo.Trim
            Else
                If requestEnt.SaleOrder.Trim = "" Then
                    WMSMsg.ShowValidate("��辺������ PO" & vbcrlf & "��� SO 㹺�����" & vbcrlf & "��سҵ�Ǩ�ͺ������ !")
                    Me.initScan()
                    Exit Sub
                Else
                    requestEnt.OrderNo = requestEnt.SaleOrder
                End If
            End If

            If IsNumeric(tmp(3).Trim) = False Then
                WMSMsg.ShowValidate("�����Ũӹǹ�Թ��ҵ�͡��ͧ㹺����� ��������Ţ !" & vbCrLf & vbCrLf & "��سҵ�Ǩ�ͺ������ : " & barcode)
                Me.initScan()
                Exit Sub
            End If
            requestEnt.ScanQty = CDbl(tmp(3).Trim)
            requestEnt.CreateBy = GlobalVariable.UserFullName
            requestEnt.Barcode = barcode

            If File.Exists(GlobalVariable.POXmlPath) = False Then
                WMSMsg.ShowValidate("��辺���������͡��çҹ�Ѻ !" & vbCrLf & "��س� Download" & vbCrLf & "�ҡ PC ��͹ !")
                Me.initScan()
                Exit Sub
            End If

            Dim sumPlanQty, sumCheckQty As Double

            Dim isFound As Boolean = False
            '����բ����� PO 㹺����鴷�������ҡ��Ǩ�ͺ XML �ҡ PO
            If requestEnt.PONo.Trim <> "" Then
                For Each dr As DataRow In GlobalVariable.dsGR.Tables(0).Rows

                    If dr("OrderNo").ToString.Trim.ToUpper = requestEnt.OrderNo.Trim.ToUpper _
                    And dr("Section").ToString.Trim.ToUpper = requestEnt.Section.Trim.ToUpper _
                    And dr("ItemCode").ToString.Trim.ToUpper = requestEnt.ItemCode.Trim.ToUpper _
                    And dr("Location").ToString.Trim.ToUpper = requestEnt.Location.Trim.ToUpper _
                    And (CDbl(dr("CheckQty")) + requestEnt.ScanQty) <= CDbl(dr("PlanQty")) Then

                        isFound = True

                        dr.BeginEdit()
                        If (CDbl(dr("CheckQty")) + requestEnt.ScanQty) = CDbl(dr("CheckQty")) Then
                            dr("CompleteBy") = GlobalVariable.UserFullName
                            dr("CompleteDate") = Format(Date.Now, "dd/MM/yyyy HH:mm:ss")
                        End If

                        dr("CheckQty") = CDbl(dr("CheckQty")) + requestEnt.ScanQty
                        dr.EndEdit()
                        Me.isUpdate = True

                        Me.txtOrder.Text = requestEnt.OrderNo
                        Me.txtSection.Text = dr("Section").ToString
                        Me.txtItemName.Text = dr("ItemName").ToString
                        Me.txtLocation.Text = dr("Location").ToString
                        Me.txtScanQty.Text = requestEnt.ScanQty
                        Me.txtPlanQty.Text = dr("PlanQty").ToString
                        Me.txtCheckQty.Text = dr("CheckQty").ToString

                        Dim drScan As DataRow
                        drScan = GlobalVariable.dsScanGR.Tables(0).NewRow
                        drScan("OrderSeq") = dr("OrderSeq").ToString
                        drScan("Barcode") = requestEnt.Barcode
                        drScan("ScanBy") = GlobalVariable.UserFullName
                        drScan("ScanDate") = Format(Date.Now, "dd/MM/yyyy HH:mm:ss")
                        drScan("GRDetailID") = dr("GRDetailID").ToString
                        drScan("ScanQty") = requestEnt.ScanQty
                        GlobalVariable.dsScanGR.Tables(0).Rows.Add(drScan)

                        sumPlanQty = GlobalVariable.dsGR.Tables(0).Compute("SUM(PlanQty)", "Section = '" & requestEnt.Section & "' AND OrderNo = '" & requestEnt.OrderNo & "' AND OrderSeq = '" & dr("OrderSeq").ToString & "' AND Location = '" & requestEnt.Location & "' AND ItemCode = '" & requestEnt.ItemCode & "' ")
                        sumCheckQty = GlobalVariable.dsGR.Tables(0).Compute("SUM(CheckQty)", "Section = '" & requestEnt.Section & "' AND OrderNo = '" & requestEnt.OrderNo & "' AND OrderSeq = '" & dr("OrderSeq").ToString & "' AND Location = '" & requestEnt.Location & "' AND ItemCode = '" & requestEnt.ItemCode & "' ")

                        If sumPlanQty = sumCheckQty Then
                            WMSMsg.ShowInformation("�س��Ǩ�ͺ�Թ���" & vbCrLf & Me.txtItemName.Text.Trim & vbCrLf & "����ͧ��: " & Me.txtLocation.Text & vbCrLf & "�Ţ����͡��� :" & vbCrLf & requestEnt.OrderNo & vbCrLf & "˹��§ҹ : " & requestEnt.Section & vbCrLf & "�ú���� !")
                        End If

                        Me.writeXML()
                        Me.initScan()
                        Exit For
                    End If
                Next
            Else
                '-------------------------------------------------------------------
                '�������բ����� PO ���� SO 㹺����鴷�������ҡ��Ǩ�ͺ XML �ҡ SO
                For Each dr As DataRow In GlobalVariable.dsGR.Tables(0).Rows

                    If dr("SaleOrder").ToString.Trim.ToUpper = requestEnt.OrderNo.Trim.ToUpper _
                    And dr("Section").ToString.Trim.ToUpper = requestEnt.Section.Trim.ToUpper _
                    And dr("ItemCode").ToString.Trim.ToUpper = requestEnt.ItemCode.Trim.ToUpper _
                    And dr("Location").ToString.Trim.ToUpper = requestEnt.Location.Trim.ToUpper _
                    And (CDbl(dr("CheckQty")) + requestEnt.ScanQty) <= CDbl(dr("PlanQty")) Then

                        isFound = True

                        dr.BeginEdit()
                        If (CDbl(dr("CheckQty")) + requestEnt.ScanQty) = CDbl(dr("CheckQty")) Then
                            dr("CompleteBy") = GlobalVariable.UserFullName
                            dr("CompleteDate") = Format(Date.Now, "dd/MM/yyyy HH:mm:ss")
                        End If

                        dr("CheckQty") = CDbl(dr("CheckQty")) + requestEnt.ScanQty
                        dr.EndEdit()
                        Me.isUpdate = True

                        Me.txtOrder.Text = requestEnt.OrderNo
                        Me.txtSection.Text = dr("Section").ToString
                        Me.txtItemName.Text = dr("ItemName").ToString
                        Me.txtLocation.Text = dr("Location").ToString
                        Me.txtScanQty.Text = requestEnt.ScanQty
                        Me.txtPlanQty.Text = dr("PlanQty").ToString
                        Me.txtCheckQty.Text = dr("CheckQty").ToString

                        Dim drScan As DataRow
                        drScan = GlobalVariable.dsScanGR.Tables(0).NewRow
                        drScan("OrderSeq") = dr("OrderSeq").ToString
                        drScan("Barcode") = requestEnt.Barcode
                        drScan("ScanBy") = GlobalVariable.UserFullName
                        drScan("ScanDate") = Format(Date.Now, "dd/MM/yyyy HH:mm:ss")
                        drScan("GRDetailID") = dr("GRDetailID").ToString
                        drScan("ScanQty") = requestEnt.ScanQty
                        GlobalVariable.dsScanGR.Tables(0).Rows.Add(drScan)


                        sumPlanQty = GlobalVariable.dsGR.Tables(0).Compute("SUM(PlanQty)", "Section = '" & requestEnt.Section & "' AND OrderNo = '" & requestEnt.OrderNo & "' AND OrderSeq = '" & dr("OrderSeq").ToString & "' AND Location = '" & requestEnt.Location & "' AND ItemCode = '" & requestEnt.ItemCode & "' ")
                        sumCheckQty = GlobalVariable.dsGR.Tables(0).Compute("SUM(CheckQty)", "Section = '" & requestEnt.Section & "' AND OrderNo = '" & requestEnt.OrderNo & "' AND OrderSeq = '" & dr("OrderSeq").ToString & "' AND Location = '" & requestEnt.Location & "' AND ItemCode = '" & requestEnt.ItemCode & "' ")

                        If sumPlanQty = sumCheckQty Then
                            WMSMsg.ShowInformation("�س��Ǩ�ͺ�Թ���" & vbCrLf & Me.txtItemName.Text.Trim & vbCrLf & "����ͧ��: " & Me.txtLocation.Text & vbCrLf & "�Ţ����͡��� :" & vbCrLf & requestEnt.OrderNo & vbCrLf & "˹��§ҹ : " & requestEnt.Section & vbCrLf & "�ú���� !")
                        End If

                        Me.writeXML()
                        Me.initScan()
                        Exit For
                    End If
                Next
            End If



            If isFound = False Then
                WMSMsg.ShowValidate("�����ź����鴷��س�᡹" & vbCrLf & _
                "˹��§ҹ : " & requestEnt.Section & vbCrLf & _
                "�����Թ��� : " & requestEnt.ItemCode & vbCrLf & _
                "��ͧ�� : " & requestEnt.Location & vbCrLf & _
                "�ӹǹ : " & requestEnt.ScanQty & vbCrLf & _
               "���ç�ѺἹ ��سҵ�Ǩ�ͺ�ա���� !")
                Me.initScan()
            End If

        Catch ex As Exception
            WMSMsg.ShowErrorTryCatch("����᡹��Ǩ�ͺ�Թ��� �Դ��Ҵ !", ex)
        Finally
            Cursor.Current = Cursors.Default
        End Try
    End Sub

    Private Sub btnOK_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnOK.Click
        Dim f As New frmGRViewDetail
        f.txtOrder.Text = Me.txtOrder.Text
        f.ShowDialog()
        If f.isUpdate = True Then
            Me.writeXML()
            Me.isUpdate = True
        End If
        Me.initCtrl()
        Me.initScan()
    End Sub

End Class