Imports System.Data

Public Class BarcodeEntity


    Private _OrderNo As String
    Private _Section As String
    Private _Status As String


    Private _ItemCode As String

    Private _ScanQty As Double

    Private _PlanQty As Double

    Private _CheckQty As Double


    Private _Location As String

    Private _Barcode As String


    Private _IsConfirm As Boolean

    Private _ConfirmRequest As Boolean


    Private _PONo As String

    Private _SaleOrder As String
    Public Property SaleOrder() As String
        Get
            Return _SaleOrder
        End Get
        Set(ByVal value As String)
            _SaleOrder = value
        End Set
    End Property

    Public Property PONo() As String
        Get
            Return _PONo
        End Get
        Set(ByVal value As String)
            _PONo = value
        End Set
    End Property

    Public Property ConfirmRequest() As Boolean
        Get
            Return _ConfirmRequest
        End Get
        Set(ByVal value As Boolean)
            _ConfirmRequest = value
        End Set
    End Property

    Public Property IsConfirm() As Boolean
        Get
            Return _IsConfirm
        End Get
        Set(ByVal value As Boolean)
            _IsConfirm = value
        End Set
    End Property

    Public Property Barcode() As String
        Get
            Return _Barcode
        End Get
        Set(ByVal value As String)
            _Barcode = value
        End Set
    End Property


    Public Property Location() As String
        Get
            Return _Location
        End Get
        Set(ByVal value As String)
            _Location = value
        End Set
    End Property


    Public Property CheckQty() As Double
        Get
            Return _CheckQty
        End Get
        Set(ByVal value As Double)
            _CheckQty = value
        End Set
    End Property

    Public Property PlanQty() As Double
        Get
            Return _PlanQty
        End Get
        Set(ByVal value As Double)
            _PlanQty = value
        End Set
    End Property

    Public Property ScanQty() As Double
        Get
            Return _ScanQty
        End Get
        Set(ByVal value As Double)
            _ScanQty = value
        End Set
    End Property

    Public Property ItemCode() As String
        Get
            Return _ItemCode
        End Get
        Set(ByVal value As String)
            _ItemCode = value
        End Set
    End Property


    Public Property Status() As String
        Get
            Return _Status
        End Get
        Set(ByVal value As String)
            _Status = value
        End Set
    End Property

    Public Property Section() As String
        Get
            Return _Section
        End Get
        Set(ByVal value As String)
            _Section = value
        End Set
    End Property

    Public Property OrderNo() As String
        Get
            Return _OrderNo
        End Get
        Set(ByVal value As String)
            _OrderNo = value
        End Set
    End Property



#Region "Common Field"
    Private _ActiveFlag As Boolean
    Private _CreateBy As String
    Private _UpdateBy As String
    Private _OldData As String
    Private _CloseBy As String
    Private _CloseDate As Date
    Private _CreateDate As Date

    Public Property CreateDate() As Date
        Get
            Return _CreateDate
        End Get
        Set(ByVal value As Date)
            _CreateDate = value
        End Set
    End Property

    Public Property CloseDate() As Date
        Get
            Return _CloseDate
        End Get
        Set(ByVal value As Date)
            _CloseDate = value
        End Set
    End Property

    Public Property CloseBy() As String
        Get
            Return _CloseBy
        End Get
        Set(ByVal value As String)
            _CloseBy = value
        End Set
    End Property

    Public Property OldData() As String
        Get
            Return _OldData
        End Get
        Set(ByVal value As String)
            _OldData = value
        End Set
    End Property

    Public Property UpdateBy() As String
        Get
            Return _UpdateBy
        End Get
        Set(ByVal value As String)
            _UpdateBy = value
        End Set
    End Property

    Public Property CreateBy() As String
        Get
            Return _CreateBy
        End Get
        Set(ByVal value As String)
            _CreateBy = value
        End Set
    End Property

    Public Property ActiveFlag() As Boolean
        Get
            Return _ActiveFlag
        End Get
        Set(ByVal value As Boolean)
            _ActiveFlag = value
        End Set
    End Property
#End Region

#Region "Query Data"
    Private _RecordDisplay As Integer
    Private _Query As String
    Private _QueryTime As String

    Public Property QueryTime() As String
        Get
            Return _QueryTime
        End Get
        Set(ByVal value As String)
            _QueryTime = value
        End Set
    End Property

    Public Property Query() As String
        Get
            Return _Query
        End Get
        Set(ByVal value As String)
            _Query = value
        End Set
    End Property

    Public Property RecordDisplay() As Integer
        Get
            Return _RecordDisplay
        End Get
        Set(ByVal value As Integer)
            _RecordDisplay = value
        End Set
    End Property
#End Region

#Region "List DataSet"
    Private _ListGetData As DataSet
    Private _ListConfirmData As DataSet

    Public Property ListGetData() As DataSet
        Get
            Return _ListGetData
        End Get
        Set(ByVal value As DataSet)
            _ListGetData = value
        End Set
    End Property

    Public Property ListConfirmData() As DataSet
        Get
            Return _ListConfirmData
        End Get
        Set(ByVal value As DataSet)
            _ListConfirmData = value
        End Set
    End Property

#End Region

#Region "List DataTable"
    Private _ListGetDataTable As DataTable
    Private _ListConfirmDataTable As DataTable

    Public Property ListGetDataTable() As DataTable
        Get
            Return _ListGetDataTable
        End Get
        Set(ByVal value As DataTable)
            _ListGetDataTable = value
        End Set
    End Property

    Public Property ListConfirmDataTable() As DataTable
        Get
            Return _ListConfirmDataTable
        End Get
        Set(ByVal value As DataTable)
            _ListConfirmDataTable = value
        End Set
    End Property

#End Region

#Region "Status"
    Private _IsComplete As Boolean
    Private _ErrorMessage As String
    Public Property IsComplete() As Boolean
        Get
            If _ErrorMessage = String.Empty Then
                _IsComplete = True
            Else
                _IsComplete = False
            End If
            Return _IsComplete
        End Get
        Set(ByVal value As Boolean)
            _IsComplete = value
        End Set
    End Property

    Public Property ErrorMessage() As String
        Get
            Return _ErrorMessage
        End Get
        Set(ByVal value As String)
            _ErrorMessage = value
        End Set
    End Property
#End Region

End Class


