Imports System.Data
Imports System.Xml
Imports System.IO


Public Class frmCountScan

    Public isUpdate As Boolean = False

    Private Sub frmCountScan_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Me.loadXmlCount()
        Me.initCtrl()
        Me.initScanLoc()
    End Sub

    Private Sub initScanLoc()
        CtrlUtil.lockCtrl(Me.txtScanLoc, False)
        CtrlUtil.lockCtrl(Me.txtScan, True)
        Me.txtScanLoc.Text = String.Empty
        Me.txtScanLoc.Focus()
    End Sub

    Private Sub initScan()
        CtrlUtil.lockCtrl(Me.txtScanLoc, True)
        CtrlUtil.lockCtrl(Me.txtScan, False)
        Me.txtScan.Text = String.Empty
        Me.txtScan.Focus()
    End Sub

    Private Sub initCtrl()
        Me.txtSection.Text = String.Empty
        Me.txtItemName.Text = String.Empty
        Me.txtLocation.Text = String.Empty
        Me.txtScanQty.Text = String.Empty
        Me.txtCountQty.Text = String.Empty
    End Sub

    Private Sub btnExit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnExit.Click
        Me.writeXML()
        Me.DialogResult = Windows.Forms.DialogResult.Cancel
    End Sub

    Private Sub writeXML()
        Try
            If GlobalVariable.dsCount.Tables(0).Rows.Count = 0 Then
                GlobalVariable.dsCount.WriteXml(GlobalVariable.CountScanXMLPath, XmlWriteMode.WriteSchema)
                ' GlobalVariable.dsCountDetail.WriteXml(GlobalVariable.CountDetailXMLPath, XmlWriteMode.WriteSchema)
            Else
                GlobalVariable.dsCount.WriteXml(GlobalVariable.CountScanXMLPath)
                ' GlobalVariable.dsCountDetail.WriteXml(GlobalVariable.CountDetailXMLPath)
            End If
        Catch ex As Exception
            WMSMsg.ShowErrorTryCatch("�����¹������ŧ XML ��� �Դ��Ҵ !", ex)
        End Try
    End Sub

    'Public Function getAllData() As DataSet
    '    Try
    '        If File.Exists("tbt_GIScan.xml") = False Then
    '            Dim dt As New DataTable("tbt_GIScan")
    '            'Create XML Config for config
    '            With dt
    '                .Columns.Add(New DataColumn("RackID", GetType(String)))
    '                .Columns.Add(New DataColumn("ScanDatetime", GetType(String)))
    '                .Columns.Add(New DataColumn("ScanDatetimeShow", GetType(String)))
    '                .Columns.Add(New DataColumn("OrderNo", GetType(String)))
    '                .Columns.Add(New DataColumn("ProductCode", GetType(String)))
    '                .Columns.Add(New DataColumn("CustomerCode", GetType(String)))
    '            End With

    '            Dim ds As New DataSet
    '            ds.Tables.Add(dt)
    '            ds.WriteXml("tbt_GIScan.xml", XmlWriteMode.WriteSchema)
    '            Return ds
    '        Else
    '            'Read XML GI Data
    '            Dim ds As New DataSet
    '            ds.ReadXml("tbt_GIScan.xml")
    '            Return ds
    '        End If
    '    Catch ex As Exception
    '        Throw New Exception("Get all data error !" & vbCrLf & ex.ToString)
    '    End Try
    'End Function

    Private Sub loadXmlCount()
        Try
            Cursor.Current = Cursors.WaitCursor

            If File.Exists(GlobalVariable.CountScanXMLPath) = False Then
                Dim dt As New DataTable("TableScanCount")
                With dt
                    .Columns.Add(New DataColumn("Section", GetType(String)))
                    .Columns.Add(New DataColumn("ItemCode", GetType(String)))
                    .Columns.Add(New DataColumn("Location", GetType(String)))
                    .Columns.Add(New DataColumn("CountQty", GetType(Integer)))
                End With
                GlobalVariable.dsCount = New DataSet
                GlobalVariable.dsCount.Tables.Add(dt.Copy)
                GlobalVariable.dsCount.WriteXml(GlobalVariable.CountScanXMLPath, XmlWriteMode.WriteSchema)
            Else
                GlobalVariable.dsCount = New DataSet
                GlobalVariable.dsCount.ReadXml(GlobalVariable.CountScanXMLPath)
            End If

            If File.Exists(GlobalVariable.CountDetailXMLPath) = False Then
                Dim dt As New DataTable("TableCountDetail")
                With dt
                    .Columns.Add(New DataColumn("Section", GetType(String)))
                    .Columns.Add(New DataColumn("Barcode", GetType(String)))
                    .Columns.Add(New DataColumn("ItemCode", GetType(String)))
                    .Columns.Add(New DataColumn("Location", GetType(String)))
                    .Columns.Add(New DataColumn("ScanQty", GetType(Integer)))
                    .Columns.Add(New DataColumn("ScanBy", GetType(String)))
                    .Columns.Add(New DataColumn("ScanDate", GetType(String)))

                End With
                GlobalVariable.dsCountDetail = New DataSet
                GlobalVariable.dsCountDetail.Tables.Add(dt.Copy)
                GlobalVariable.dsCountDetail.WriteXml(GlobalVariable.CountDetailXMLPath, XmlWriteMode.WriteSchema)
            Else
                GlobalVariable.dsCountDetail = New DataSet
                GlobalVariable.dsCountDetail.ReadXml(GlobalVariable.CountDetailXMLPath)

            End If
        Catch ex As Exception
            WMSMsg.ShowErrorTryCatch("�����Ŵ�����š�ùѺ" & vbCrLf & "�Դ��Ҵ !", ex)
        Finally
            Cursor.Current = Cursors.Default
        End Try
    End Sub

    Private Sub scanCount(ByVal barcode As String)
        Try
            Me.initCtrl()

            Dim tmp() As String
            tmp = barcode.Split("$")
            'If tmp.Length <> 4 Then
            '    WMSMsg.ShowValidate("�����鴼Դ�ٻẺ !" & vbCrLf & vbCrLf & "��سҵ�Ǩ�ͺ������ : " & barcode)
            '    Me.initScan()
            '    Exit Sub
            'End If

            If tmp.Length <> 8 Then
                WMSMsg.ShowValidate("�����鴼Դ�ٻẺ !" & vbCrLf & vbCrLf & "��سҵ�Ǩ�ͺ������ : " & barcode)
                Me.initScan()
                Exit Sub
            End If

            Dim requestEnt As New BarcodeEntity
            '˹��§ҹ$�����Թ���$�����$�ӹǹ��͡��ͧ
            requestEnt.Section = tmp(0).ToString.Trim
            requestEnt.ItemCode = tmp(1).ToString.Trim
            requestEnt.Location = tmp(2).ToString.Trim
            If IsNumeric(tmp(3).Trim) = False Then
                WMSMsg.ShowValidate("�����Ũӹǹ�Թ��ҵ�͡��ͧ㹺����� ��������Ţ !" & vbCrLf & vbCrLf & "��سҵ�Ǩ�ͺ������ : " & barcode)
                Me.initScan()
                Exit Sub
            End If
            requestEnt.ScanQty = CDbl(tmp(3).Trim)
            requestEnt.CreateBy = GlobalVariable.UserFullName
            requestEnt.Barcode = barcode

            If File.Exists(GlobalVariable.CountScanXMLPath) = False Then
                WMSMsg.ShowValidate("��辺�������š�ùѺ�Թ��� !" & vbCrLf & "��س� Download" & vbCrLf & "�ҡ PC ��͹ !")
                Me.initScan()
                Exit Sub
            End If

            If Me.txtScanLoc.Text.Trim.ToUpper <> requestEnt.Location.Trim.ToUpper Then
                WMSMsg.ShowValidate("�����Ū�ͧ��㹺�����" & vbCrLf & "��� : " & requestEnt.Location & vbCrLf & "���ç�Ѻ��ͧ�纷��س�᡹" & vbCrLf & "��سҵ�Ǩ�ͺ�ա���� !")
                Me.initScan()
                Exit Sub
            End If

            If GlobalVariable.dsCount.Tables(0).Rows.Count = 0 Then
                Dim drNew As DataRow
                drNew = GlobalVariable.dsCount.Tables(0).NewRow
                drNew("Section") = requestEnt.Section
                drNew("ItemCode") = requestEnt.ItemCode
                drNew("Location") = requestEnt.Location
                drNew("CountQty") = requestEnt.ScanQty
                GlobalVariable.dsCount.Tables(0).Rows.Add(drNew)
                GlobalVariable.dsCount.Tables(0).AcceptChanges()

                'Dim drScan As DataRow
                'drScan = GlobalVariable.dsCountDetail.Tables(0).NewRow
                'drScan("Section") = requestEnt.Section
                'drScan("Barcode") = requestEnt.Barcode
                'drScan("Location") = Me.txtLocation.Text
                'drScan("ScanQty") = requestEnt.ScanQty
                'drScan("ItemCode") = requestEnt.ItemCode
                'drScan("ScanBy") = GlobalVariable.UserFullName
                'drScan("ScanDate") = Format(Date.Now, "dd/MM/yyyy HH:mm:ss")
                'GlobalVariable.dsCountDetail.Tables(0).Rows.Add(drScan)

                Me.txtSection.Text = requestEnt.Section
                Me.txtItemName.Text = requestEnt.ItemCode
                Me.txtLocation.Text = requestEnt.Location
                Me.txtScanQty.Text = requestEnt.ScanQty
                Me.txtCountQty.Text = requestEnt.ScanQty

            Else '===================================================================================
                Dim isFound As Boolean = False
                Dim count As Integer
                count = GlobalVariable.dsCount.Tables(0).Rows.Count - 1
                For i As Integer = 0 To count
                    If GlobalVariable.dsCount.Tables(0).Rows(i).Item("Section").ToString.Trim.ToUpper = requestEnt.Section.Trim.ToUpper _
                    And GlobalVariable.dsCount.Tables(0).Rows(i).Item("ItemCode").ToString.Trim.ToUpper = requestEnt.ItemCode.Trim.ToUpper _
                    And GlobalVariable.dsCount.Tables(0).Rows(i).Item("Location").ToString.Trim.ToUpper = requestEnt.Location.Trim.ToUpper Then

                        Dim dr As DataRow
                        dr = GlobalVariable.dsCount.Tables(0).Rows(i)

                        dr.BeginEdit()

                        dr("CountQty") = CDbl(dr("CountQty")) + requestEnt.ScanQty
                        dr.EndEdit()
                        GlobalVariable.dsCount.Tables(0).AcceptChanges()

                        Me.isUpdate = True

                        Me.txtSection.Text = dr("Section").ToString
                        Me.txtItemName.Text = requestEnt.ItemCode
                        Me.txtLocation.Text = dr("Location").ToString
                        Me.txtScanQty.Text = requestEnt.ScanQty
                        Me.txtCountQty.Text = dr("CountQty").ToString

                        isFound = True
                        Exit For

                    End If

                Next i

                If isFound = False Then
                    Dim drNew As DataRow
                    drNew = GlobalVariable.dsCount.Tables(0).NewRow
                    drNew("Section") = requestEnt.Section
                    drNew("ItemCode") = requestEnt.ItemCode
                    drNew("Location") = requestEnt.Location
                    drNew("CountQty") = requestEnt.ScanQty
                    GlobalVariable.dsCount.Tables(0).Rows.Add(drNew)
                    GlobalVariable.dsCount.Tables(0).AcceptChanges()

                    Me.txtSection.Text = requestEnt.Section
                    Me.txtItemName.Text = requestEnt.ItemCode
                    Me.txtLocation.Text = requestEnt.Location
                    Me.txtScanQty.Text = requestEnt.ScanQty
                    Me.txtCountQty.Text = requestEnt.ScanQty
                End If

                'Dim drScan As DataRow
                'drScan = GlobalVariable.dsCountDetail.Tables(0).NewRow
                'drScan("Section") = requestEnt.Section
                'drScan("Barcode") = requestEnt.Barcode
                'drScan("Location") = Me.txtLocation.Text
                'drScan("ScanQty") = CInt(Me.txtScanQty.Text)
                'drScan("ItemCode") = requestEnt.ItemCode
                'drScan("ScanBy") = GlobalVariable.UserFullName
                'drScan("ScanDate") = Format(Date.Now, "dd/MM/yyyy HH:mm:ss")
                'GlobalVariable.dsCountDetail.Tables(0).Rows.Add(drScan)

            End If
            Me.writeXML()
            Me.initScan()

        Catch ex As Exception
            WMSMsg.ShowErrorTryCatch("����᡹�Ѻ�Թ��� �Դ��Ҵ !", ex)
        Finally
            Cursor.Current = Cursors.Default
        End Try
    End Sub

    Private Sub txtScanLoc_KeyPress(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txtScanLoc.KeyPress
        If e.KeyChar = Chr(Keys.Enter) Then
            If CtrlUtil.checkEnterData(Me.txtScanLoc) = False Then
                Exit Sub
            End If
            Try
                Me.initScan()
            Catch ex As Exception
                WMSMsg.ShowErrorTryCatch("����᡹��ͧ�� �Դ��Ҵ !", ex)
            End Try
        End If
    End Sub
  
    Private Sub txtScan_KeyPress(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txtScan.KeyPress
        If e.KeyChar = Chr(Keys.Enter) Then
            If CtrlUtil.checkEnterData(Me.txtScan) = False Then
                Exit Sub
            End If
            Me.scanCount(Me.txtScan.Text.Trim)
        End If
    End Sub

   
    Private Sub btnChangeLoc_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnChangeLoc.Click
        Me.initCtrl()
        Me.initScan()
        Me.initScanLoc()
    End Sub

    Private Sub btnOK_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnOK.Click
        Dim f As New frmCountViewDetail
        f.ShowDialog()
        Me.initCtrl()

        If Me.txtScanLoc.ReadOnly = False Then
            Me.initScanLoc()
        Else
            Me.initScan()
        End If


    End Sub
End Class