Imports System.Data

Public Class frmCountViewDetail

    Public isUpdate As Boolean = False

    Private Sub frmCountViewDetail_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Me.showGridDetail()
    End Sub

    Private Sub showGridDetail()
        Try
            Cursor.Current = Cursors.WaitCursor
            Me.dgvDetail.DataSource = GlobalVariable.dsCount.Tables(0)
        Catch ex As Exception
            WMSMsg.ShowErrorTryCatch("����ʴ������š�ùѺ �Դ��Ҵ !", ex)
        Finally
            Cursor.Current = Cursors.Default
        End Try
    End Sub

    Private Sub btnExit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnExit.Click
        Me.DialogResult = Windows.Forms.DialogResult.No
    End Sub

    Private Sub btnCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        Me.cancelData()
    End Sub
    Private Sub cancelData()
        Try
            Cursor.Current = Cursors.WaitCursor

            If GlobalVariable.dsCount.Tables(0).Rows.Count = 0 Then
                OGAMsg.ShowValidate("��辺�����š�ùѺ�Թ��� !")
                Exit Sub
            End If


            Dim section, location, itemCode As String
            section = Me.dgvDetail.Item(Me.dgvDetail.CurrentCell.RowNumber, 0).ToString.ToUpper
            location = Me.dgvDetail.Item(Me.dgvDetail.CurrentCell.RowNumber, 1).ToString.ToUpper
            itemCode = Me.dgvDetail.Item(Me.dgvDetail.CurrentCell.RowNumber, 2).ToString.ToUpper

            If WMSMsg.ShowConfirm("�س��ͧ���¡��ԡ" & vbCrLf & "�ӹǹ�Ѻ" & vbCrLf & _
            "�Թ��� : " & itemCode & vbCrLf & "��ͧ�� : " & location & vbCrLf & "˹��§ҹ : " & section & vbCrLf & "�� ���� ��� ?") <> Windows.Forms.DialogResult.Yes Then
                Exit Sub
            End If

            For i As Integer = 0 To GlobalVariable.dsCount.Tables(0).Rows.Count - 1
                If GlobalVariable.dsCount.Tables(0).Rows(i).Item("Section").ToString.ToUpper = section _
                    And GlobalVariable.dsCount.Tables(0).Rows(i).Item("Location").ToString.ToUpper = location _
                    And GlobalVariable.dsCount.Tables(0).Rows(i).Item("ItemCode").ToString.ToUpper = itemCode Then
                    GlobalVariable.dsCount.Tables(0).Rows(i).Delete()
                    GlobalVariable.dsCount.Tables(0).AcceptChanges()
                    Exit For
                End If
            Next
            Me.showGridDetail()
            WMSMsg.ShowResult("���¡��ԡ�ӹǹ�Ѻ" & vbCrLf & "�Թ��� : " & itemCode & vbCrLf & "��ͧ�� : " & location & vbCrLf & "˹��§ҹ : " & section & " ��������ó� !")

        Catch ex As Exception
            OGAMsg.ShowErrorTryCatch("���¡��ԡ�ӹǹ�Ѻ �Դ��Ҵ !", ex)
        Finally
            Cursor.Current = Cursors.Default
        End Try
    End Sub

    Private Sub btnCancelAll_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCancelAll.Click
        Me.cancelAll()
    End Sub

    Private Sub cancelAll()
        Try
            Cursor.Current = Cursors.WaitCursor

            If GlobalVariable.dsCount.Tables(0).Rows.Count = 0 Then
                OGAMsg.ShowValidate("��辺�����š�ùѺ�Թ��� !")
                Exit Sub
            End If


            Dim section, location, itemCode As String
            section = Me.dgvDetail.Item(Me.dgvDetail.CurrentCell.RowNumber, 0).ToString.ToUpper
            location = Me.dgvDetail.Item(Me.dgvDetail.CurrentCell.RowNumber, 1).ToString.ToUpper
            itemCode = Me.dgvDetail.Item(Me.dgvDetail.CurrentCell.RowNumber, 2).ToString.ToUpper

            If WMSMsg.ShowConfirm("�س��ͧ���¡��ԡ�ӹǹ�Ѻ" & vbCrLf & "������ �� ���� ��� ?") <> Windows.Forms.DialogResult.Yes Then
                Exit Sub
            End If

            GlobalVariable.dsCount.Tables(0).Rows.Clear()
            GlobalVariable.dsCount.Tables(0).AcceptChanges()
          
            Me.showGridDetail()
            WMSMsg.ShowResult("���¡��ԡ�ӹǹ�Ѻ������" & vbCrLf & "��������ó� !")

        Catch ex As Exception
            OGAMsg.ShowErrorTryCatch("���¡��ԡ�ӹǹ�Ѻ������ �Դ��Ҵ !", ex)
        Finally
            Cursor.Current = Cursors.Default
        End Try
    End Sub
End Class