Imports System.Xml
Imports System.Data
Imports System.IO


Public Class frmGRSelectOrder

    Private Sub frmGRSelectOrder_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Me.loadXmlPO()
    End Sub

    Private Sub btnExit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnExit.Click
        Me.DialogResult = Windows.Forms.DialogResult.Cancel
    End Sub

    Private Sub loadXmlPO()
        Try
            Cursor.Current = Cursors.WaitCursor

            If File.Exists(GlobalVariable.POXmlPath) = False Then
                WMSMsg.ShowValidate("��辺��� XML ������" & vbCrLf & "�ҹ�Ѻ�Թ��� !" & vbCrLf & "��س� Download" & vbCrLf & "�ҡ PC ��͹")
                Exit Sub
            End If

            GlobalVariable.dsGR = New DataSet
            GlobalVariable.dsGR.ReadXml(GlobalVariable.POXmlPath)

            Me.loadXmlScanGR()

            Me.showComboOrder()
        Catch ex As Exception
            WMSMsg.ShowErrorTryCatch("�����Ŵ�������͡���" & vbCrLf & "����Ѻ �Դ��Ҵ !", ex)
        Finally
            Cursor.Current = Cursors.Default
        End Try
    End Sub


    Private Sub loadXmlScanGR()
        Try
            Cursor.Current = Cursors.WaitCursor

            If File.Exists(GlobalVariable.POScanXmlPath) = False Then
                WMSMsg.ShowValidate("��辺��� XML ������" & vbCrLf & "����᡹��Ǩ�ͺ�Թ���!" & vbCrLf & "��س� Download" & vbCrLf & "�ҡ PC ��͹")
                Exit Sub
            End If

            GlobalVariable.dsScanGR = New DataSet
            GlobalVariable.dsScanGR.ReadXml(GlobalVariable.POScanXmlPath)

            Me.showComboOrder()
        Catch ex As Exception
            WMSMsg.ShowErrorTryCatch("�����Ŵ�����š���᡹" & vbCrLf & "��Ǩ�Ѻ�Թ��� �Դ��Ҵ !", ex)
        Finally
            Cursor.Current = Cursors.Default
        End Try
    End Sub

    Private Sub showComboOrder()
        Try
            Cursor.Current = Cursors.WaitCursor
            Dim dt As DataTable = GlobalVariable.dsGR.Tables(0).DefaultView.ToTable(True, "OrderNo")
            ComboService.BindComboData(Me.cmbOrder, dt, "OrderNo", "OrderNo")
        Catch ex As Exception
            WMSMsg.ShowErrorTryCatch("����ʴ���¡���͡��� �Դ��Ҵ !", ex)
        Finally
            Cursor.Current = Cursors.Default
        End Try
    End Sub

    Private Sub btnOK_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnOK.Click
        If CtrlUtil.checkSelectData(Me.cmbOrder) = False Then
            Exit Sub
        End If
        Dim f As New frmGRScanConfirm
        f.txtOrder.Text = Me.cmbOrder.SelectedValue.ToString.Trim
        f.ShowDialog()
        If f.isUpdate = True Then
            Me.loadXmlPO()
        Else
            Me.cmbOrder.SelectedIndex = -1
            Me.cmbOrder.SelectedIndex = -1
        End If
        
    End Sub

End Class