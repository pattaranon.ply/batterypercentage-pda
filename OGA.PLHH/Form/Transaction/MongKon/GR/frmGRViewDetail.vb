Imports System.Data


Public Class frmGRViewDetail

    Public isUpdate As Boolean = False
    Public orderSeq As String = ""

    Private Sub frmGRViewDetail_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Me.showGridDetail()
    End Sub

    Private Sub showGridDetail()
        Try
            Cursor.Current = Cursors.WaitCursor
            Dim dv As New DataView
            dv = GlobalVariable.dsGR.Tables(0).DefaultView
            dv.RowFilter = "OrderNo = '" & Me.txtOrder.Text.Trim & "' AND Section = '" & Me.txtSection.Text.Trim & "' "
            Me.dgvDetail.DataSource = dv
            DataGridEnableEventArgs.addGridStyle(Me.dgvDetail, dv.ToTable, Me.DataGridTableStyle1, "PlanQty", "CheckQty", False)
        Catch ex As Exception
            WMSMsg.ShowErrorTryCatch("����ʴ������š���Ѻ �Դ��Ҵ !", ex)
        Finally
            Cursor.Current = Cursors.Default
        End Try
    End Sub

    Private Sub btnExit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnExit.Click
        Me.DialogResult = Windows.Forms.DialogResult.Cancel
    End Sub


    Private Sub btnCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        Me.cancelData()
    End Sub

    Private Sub btnCancelAll_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCancelAll.Click
        If WMSMsg.ShowConfirm("�س��ͧ���¡��ԡ" & vbCrLf & "�ӹǹ��Ǩ�ͺ�Թ���" & vbCrLf & "�͡��� : " & Me.txtOrder.Text & vbCrLf & vbCrLf & "˹��§ҹ : " & Me.txtSection.Text.Trim & vbCrLf & "������ �� ���� ��� ?") = Windows.Forms.DialogResult.Yes Then
            Me.cancelAll()
        End If
    End Sub

    Private Sub cancelData()
        Try
            Cursor.Current = Cursors.WaitCursor

            If GlobalVariable.dsGR.Tables(0).Rows.Count = 0 Then
                OGAMsg.ShowValidate("��辺�����š���Ѻ�Թ��� !")
                Exit Sub
            End If


            Dim detailID, lineNo, itemName As String
            itemName = Me.dgvDetail.Item(Me.dgvDetail.CurrentCell.RowNumber, 2).ToString
            detailID = Me.dgvDetail.Item(Me.dgvDetail.CurrentCell.RowNumber, 5).ToString
            lineNo = Me.dgvDetail.CurrentCell.RowNumber + 1

            If WMSMsg.ShowConfirm("�س��ͧ���¡��ԡ" & vbCrLf & "�ӹǹ�᡹��Ǩ�ͺ" & vbCrLf & _
            "�Թ��� : " & itemName & vbCrLf & "��÷Ѵ��� : " & lineNo & " �� ���� ��� ?") <> Windows.Forms.DialogResult.Yes Then
                Exit Sub
            End If

            'Dim drDelete As New DataRow
            For Each dr As DataRow In GlobalVariable.dsGR.Tables(0).Rows
                If dr("GRDetailID").ToString.Trim = detailID Then
                    dr.BeginEdit()
                    dr("CheckQty") = 0
                    dr("CompleteBy") = "-"
                    dr("CompleteDate") = "-"
                    dr.EndEdit()
                    GlobalVariable.dsGR.Tables(0).AcceptChanges()
                    Me.isUpdate = True
                    Exit For
                End If
            Next dr

            Dim count As Integer
            Dim index As Integer = 0
            count = GlobalVariable.dsScanGR.Tables(0).Rows.Count - 1
            For j As Integer = 0 To count
                If GlobalVariable.dsScanGR.Tables(0).Rows(index).Item("GRDetailID") = detailID Then
                    GlobalVariable.dsScanGR.Tables(0).Rows(index).Delete()
                    GlobalVariable.dsScanGR.Tables(0).AcceptChanges()
                    index = index
                Else
                    index = index + 1
                End If

            Next j


            Me.showGridDetail()
            WMSMsg.ShowResult("���¡��ԡ�ӹǹ��Ǩ�ͺ" & vbCrLf & "�Թ��� : " & itemName & vbCrLf & "��÷Ѵ��� : " & lineNo & " ��������ó� !")


        Catch ex As Exception
            OGAMsg.ShowErrorTryCatch("���¡��ԡ�ӹǹ��Ǩ�ͺ�Թ��� �Դ��Ҵ !", ex)
        Finally
            Cursor.Current = Cursors.Default
        End Try
    End Sub

    Private Sub cancelAll()
        Try
            Cursor.Current = Cursors.WaitCursor

            If GlobalVariable.dsGR.Tables(0).Rows.Count = 0 Then
                OGAMsg.ShowValidate("��辺�����š���Ѻ�Թ��� !")
                Exit Sub
            End If


            'Dim drDelete As New DataRow
            For Each dr As DataRow In GlobalVariable.dsGR.Tables(0).Rows
                If dr("OrderNo").ToString.Trim.ToUpper = Me.txtOrder.Text.Trim.ToUpper Then
                    dr.BeginEdit()
                    dr("CheckQty") = 0
                    dr("CompleteBy") = "-"
                    dr("CompleteDate") = "-"
                    dr.EndEdit()
                End If
            Next dr

            GlobalVariable.dsGR.Tables(0).AcceptChanges()
            Me.isUpdate = True

            Dim count As Integer
            Dim index As Integer = 0
            count = GlobalVariable.dsScanGR.Tables(0).Rows.Count - 1
            For j As Integer = 0 To count
                If GlobalVariable.dsScanGR.Tables(0).Rows(index).Item("OrderSeq") = Me.orderSeq Then
                    GlobalVariable.dsScanGR.Tables(0).Rows(index).Delete()
                    GlobalVariable.dsScanGR.Tables(0).AcceptChanges()
                    index = index
                Else
                    index = index + 1
                End If

            Next j

            Me.showGridDetail()
            WMSMsg.ShowResult("���¡��ԡ�ӹǹ��Ǩ�ͺ" & vbCrLf & "�Թ��ҷ����� ��������ó�!")


        Catch ex As Exception
            OGAMsg.ShowErrorTryCatch("���¡��ԡ�ӹǹ��Ǩ�ͺ" & vbCrLf & "�Թ��ҷ����� �Դ��Ҵ !", ex)
        Finally
            Cursor.Current = Cursors.Default
        End Try
    End Sub
End Class