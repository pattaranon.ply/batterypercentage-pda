Imports System.Data

Public Class frmGIScan

    Public OrderNo As String
    Public CustomerCode As String
    Public ProductCode As String

    Public isOnline As Boolean

    Dim dvOrder As DataView
    Dim dvProduct As DataView

    Private Sub frmGIScan_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Me.showRackIDByOrderAndProduct()
        Me.initCtrl()
    End Sub

    Private Sub initCtrl()
        CtrlUtil.lockCtrl(Me.txtProduct, False)
        CtrlUtil.lockCtrl(Me.txtScan, True)
        Me.ProductCode = String.Empty
        Me.txtProduct.Text = String.Empty
        Me.txtScan.Text = String.Empty
        Me.txtProduct.Focus()
    End Sub

    Private Sub initCtrlRack()
        CtrlUtil.lockCtrl(Me.txtScan, False)
        Me.txtScan.Text = String.Empty
        Me.txtScan.Focus()
    End Sub

    Private Sub txtProduct_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txtProduct.KeyPress
        If e.KeyChar = Chr(Keys.Enter) Then

            If CtrlUtil.checkEnterData(Me.txtProduct, "Plaease enter Product Code !") = False Then
                Exit Sub
            End If

            If Me.txtProduct.Text.Trim.Length <> 9 Then
                OGAMsg.ShowValidate("Format Error !" & vbCrLf & "Product Code" & vbCrLf & "Length <> 9 " & vbCrLf & "Please try again !")
                Me.txtProduct.Text = String.Empty
                Me.txtProduct.Focus()
                Exit Sub
            End If

            If Me.isOnline = True Then
                'Search product data from dataset
                Dim isFoundProd As Boolean = False
                For i As Integer = 0 To GlobalVariable.dsOrder.Tables(0).Rows.Count - 1
                    If Me.txtProduct.Text.Trim.ToUpper = GlobalVariable.dsOrder.Tables(0).Rows(i).Item("LPROD").ToString.Trim.ToUpper Then
                        Me.ProductCode = GlobalVariable.dsOrder.Tables(0).Rows(i).Item("LPROD").ToString.Trim
                        'Display product name from dataset
                        Me.txtProduct.Text = GlobalVariable.dsOrder.Tables(0).Rows(i).Item("IDESC").ToString.Trim
                        isFoundProd = True
                        Exit For
                    End If
                Next i
                If isFoundProd = False Then
                    OGAMsg.ShowValidate("Not found product code :" & vbCrLf & Me.txtProduct.Text.Trim & vbCrLf & "in Order !")
                    Me.initCtrl()
                    Exit Sub
                End If


            Else '�ó� isOnline = False ��� Manual
                If Me.txtProduct.Text.Trim.Length > 15 Then
                    OGAMsg.ShowValidate("Wrong format product code !" & vbCrLf & "Please try again !")
                    Me.initCtrl()
                    Exit Sub
                End If
                Me.ProductCode = Me.txtProduct.Text.Trim

            End If  ' End If Me.isOnline = True Then

            CtrlUtil.lockCtrl(Me.txtProduct, True)
            Me.initCtrlRack()
            Me.showRackIDByOrderAndProduct()
        End If
    End Sub

    Private Sub btnNewOrder_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnNewOrder.Click
        If GlobalVariable.ConnectionStatus = "Online" Then
            If GlobalVariable.dsGI.Tables(0).Rows.Count <> 0 Then
                If OGAMsg.ShowConfirm("Do you want to finish work ?") = Windows.Forms.DialogResult.Yes Then
                    Me.confirmDataToServer(True) '
                Else
                    Me.Close()
                End If
            Else
                Me.Close()
            End If
        Else
            'Connection Status = Offline
            Me.Close()
        End If
       
    End Sub


    Private Sub txtScan_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txtScan.KeyPress
        If e.KeyChar = Chr(Keys.Enter) Then
            If CtrlUtil.checkEnterData(Me.txtScan) = False Then
                Exit Sub
            End If
            Me.InspectBarcode()
        End If

    End Sub

#Region "showRackIDByOrderAndProduct"
    Private Sub showRackIDByOrderAndProduct()
        Try
            Cursor.Current = Cursors.WaitCursor
            Dim dal As New GIDAL
            GlobalVariable.dsGI = dal.getAllData

            If GlobalVariable.dsGI.Tables(0).Rows.Count <> 0 Then

                Me.dvOrder = New DataView
                Me.dvOrder.Table = GlobalVariable.dsGI.Tables("tbt_GIScan")
                Me.dvOrder.RowFilter = "OrderNo = '" & Me.OrderNo & "'"

                If Me.dvOrder.Count <> 0 Then
                    Me.lblTotalByOrder.Text = "Total by Order : " & Me.dvOrder.Count
                Else
                    Me.lblTotalByOrder.Text = "Total by Order : 0"
                End If


                Me.dvProduct = New DataView
                Me.dvProduct.Table = GlobalVariable.dsGI.Tables("tbt_GIScan")
                Me.dvProduct.RowFilter = "OrderNo = '" & Me.OrderNo & "' AND ProductCode = '" & Me.ProductCode & "' "

                If Me.dvProduct.Count <> 0 Then
                    Me.dgDetail.DataSource = Me.dvProduct
                    Me.lblTotalByProduct.Text = "Total by Product : " & Me.dvProduct.Count
                Else
                    Me.dgDetail.DataSource = Nothing
                    Me.lblTotalByProduct.Text = "Total by Product : 0"
                End If
            Else
                Me.dgDetail.DataSource = Nothing
                Me.lblTotalByOrder.Text = "Total by Order : 0"
                Me.lblTotalByProduct.Text = "Total by Product : 0"
            End If
        Catch ex As Exception
            OGAMsg.ShowErrorTryCatch("Get Pallet data error !", ex)
        Finally
            Cursor.Current = Cursors.Default
        End Try
    End Sub
#End Region

#Region "Insert Data"
    Dim barcode As New BarcodeRackDetail()

    Private Sub InspectBarcode()
        Try
            Cursor.Current = Cursors.WaitCursor

            Me.barcode = BarcodetInspect.InspectBarcodeRack(Me.txtScan.Text.Trim)
            Dim giEnt As New GIEntity
            giEnt.RackID = Me.barcode.Barcode.ToUpper.Trim
            giEnt.ScanDatetime = Date.Now
            giEnt.ScanDatetimeShow = Format(giEnt.ScanDatetime, "dd/MM/yyyy HH:mm:ss")
            giEnt.OrderNo = Me.txtOrder.Text.Trim
            giEnt.ProductCode = Me.ProductCode.ToUpper.Trim
            giEnt.CustomerCode = Me.CustomerCode.ToUpper.Trim


            'Check scan barcode rack is duplicate before add to dataset ----------------------
            If GlobalVariable.dsGI.Tables(0).Rows.Count <> 0 Then
                For i As Integer = 0 To GlobalVariable.dsGI.Tables(0).Rows.Count - 1
                    If giEnt.RackID = GlobalVariable.dsGI.Tables(0).Rows(i).Item("RackID").ToString.Trim Then
                        OGAMsg.ShowValidate("Scan Pallet " & vbCrLf & "ID : " & giEnt.RackID & vbCrLf & _
                        "is duplicate !" & vbCrLf & "in Order : " & GlobalVariable.dsGI.Tables(0).Rows(i).Item("OrderNo").ToString.Trim & vbCrLf & "Please try again !")
                        Me.initCtrlRack()
                        Exit Sub
                    End If
                Next i
            End If
            '---------------------------------------------------------------------------------

            Dim dal As New GIDAL

            If dal.insertData(giEnt) = True Then
                Me.showRackIDByOrderAndProduct()
                Me.initCtrlRack()
            End If

        Catch exBarcode As BarcodeInspectException
            OGAMsg.ShowErrorTryCatchBarcode(exBarcode)
            Me.initCtrlRack()
        Catch ex As Exception
            OGAMsg.ShowError(ex.ToString)
            Me.initCtrlRack()
        Finally
            Cursor.Current = Cursors.Default
        End Try
    End Sub

#End Region

    Private Sub btnNewProduct_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnNewProduct.Click
        Me.initCtrl()
        Me.lblTotalByProduct.Text = "Total by Product : 0"
        Me.dgDetail.DataSource = Nothing
    End Sub

    Private Sub btnDelete_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnDelete.Click
        Me.DeleteScanData()
    End Sub

#Region "DeleteScanData"
    Private Sub DeleteScanData()
        Try

            If GlobalVariable.dsGI.Tables(0).Rows.Count = 0 Then
                OGAMsg.ShowValidate("No data for delete !")
                Exit Sub
            End If

            If Me.dvOrder.Count = 0 Then
                OGAMsg.ShowValidate("No data for delete !" & vbCrLf & "Please check again !")
                Exit Sub
            End If

            If Me.dvProduct.Count = 0 Then
                OGAMsg.ShowValidate("No data for delete !" & vbCrLf & "Please check again !")
                Exit Sub
            End If

            Cursor.Current = Cursors.WaitCursor
           

            Dim rackID As String
            rackID = Me.dgDetail.Item(Me.dgDetail.CurrentCell.RowNumber, 0).ToString

            ' Dim dr As DataRow = GlobalVariable.dsGI.Tables(0).Rows(Me.dgDetail.CurrentRowIndex)


            If OGAMsg.ShowConfirm("Are you sure to delete" & _
                      vbCrLf & "Pallet ID : " & rackID & _
                      vbCrLf & "Yes or No ?") <> Windows.Forms.DialogResult.Yes Then
                Exit Sub
            End If

            'Dim drDelete As New DataRow
            For Each dr As DataRow In GlobalVariable.dsGI.Tables(0).Rows
                If dr("RackID").ToString.Trim = rackID Then
                    Cursor.Current = Cursors.WaitCursor
                    GlobalVariable.dsGI.Tables(0).Rows.Remove(dr)
                    GlobalVariable.dsGI.Tables(0).AcceptChanges()
                    Exit For
                End If
            Next dr


            Dim dal As New GIDAL
            dal.writeForDelete()

            Me.showRackIDByOrderAndProduct()
            OGAMsg.ShowResult("Delete Pallet ID : " & rackID & vbCrLf & "completed !")


        Catch ex As Exception
            OGAMsg.ShowErrorTryCatch("Delete Pallet error !" & vbCrLf & "Error !", ex)
        Finally
            Cursor.Current = Cursors.Default
        End Try
    End Sub
#End Region

    Private Sub btnSendToServer_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSendToServer.Click
        If GlobalVariable.ConnectionStatus = "Online" Then
            If GlobalVariable.dsGI.Tables(0).Rows.Count = 0 Then
                OGAMsg.ShowValidate("No data for finish work !")
                Me.initCtrlRack()
                Exit Sub
            End If
            If OGAMsg.ShowConfirm("Are you sure to finish work ?") = Windows.Forms.DialogResult.Yes Then
                Me.confirmDataToServer(True)
            End If
        Else
            'Connection Status = Offline
            OGAMsg.ShowValidate("Can not finish work !" & vbCrLf & "It is Offline mode.")
        End If
       
    End Sub

#Region "confirmDataToServer"
    Private Sub confirmDataToServer(Optional ByVal isClose As Boolean = False)
        Try
            Cursor.Current = Cursors.WaitCursor
            Dim wsGI As WSGI.GIService
            wsGI = InstanceService.GetWSGIInstance

            Dim requestEnt As New WSGI.GIEntity
            requestEnt.ListConfirmData = GlobalVariable.dsGI

            Dim resultEnt As New WSGI.GIEntity

            resultEnt = wsGI.WriteScanDataToServer(requestEnt)

            If resultEnt.IsComplete = True Then
                Dim dal As New GIDAL
                dal.deleteAllData()
                Me.initCtrl()
                Me.showRackIDByOrderAndProduct()
                OGAMsg.ShowResult("Finish work completed !")
                If isClose = True Then
                    Me.Close()
                End If
            Else
                OGAMsg.ShowErrorFromWS(resultEnt.ErrorMessage)
                Me.initCtrl()
            End If

        Catch ex As System.Net.WebException
            OGAMsg.ShowErrorConnectWebService(ex)
        Catch ex As Exception
            OGAMsg.ShowErrorTryCatch(ex)
        Finally
            Cursor.Current = Cursors.Default
        End Try
    End Sub
#End Region
End Class