<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Public Class frmGIScan
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer
    Private mainMenu1 As System.Windows.Forms.MainMenu

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.mainMenu1 = New System.Windows.Forms.MainMenu
        Me.dgDetail = New System.Windows.Forms.DataGrid
        Me.DataGridTableStyle1 = New System.Windows.Forms.DataGridTableStyle
        Me.DataGridTextBoxColumn1 = New System.Windows.Forms.DataGridTextBoxColumn
        Me.DataGridTextBoxColumn2 = New System.Windows.Forms.DataGridTextBoxColumn
        Me.btnNewProduct = New System.Windows.Forms.Button
        Me.btnSendToServer = New System.Windows.Forms.Button
        Me.lblTotalByOrder = New System.Windows.Forms.Label
        Me.Panel1 = New System.Windows.Forms.Panel
        Me.txtScan = New System.Windows.Forms.TextBox
        Me.Label2 = New System.Windows.Forms.Label
        Me.lblTitle = New System.Windows.Forms.Label
        Me.btnDelete = New System.Windows.Forms.Button
        Me.txtOrder = New System.Windows.Forms.TextBox
        Me.Label1 = New System.Windows.Forms.Label
        Me.lblTotalByProduct = New System.Windows.Forms.Label
        Me.btnNewOrder = New System.Windows.Forms.Button
        Me.txtProduct = New System.Windows.Forms.TextBox
        Me.Label4 = New System.Windows.Forms.Label
        Me.SuspendLayout()
        '
        'dgDetail
        '
        Me.dgDetail.BackgroundColor = System.Drawing.Color.FromArgb(CType(CType(128, Byte), Integer), CType(CType(128, Byte), Integer), CType(CType(128, Byte), Integer))
        Me.dgDetail.Location = New System.Drawing.Point(5, 136)
        Me.dgDetail.Name = "dgDetail"
        Me.dgDetail.Size = New System.Drawing.Size(227, 108)
        Me.dgDetail.TabIndex = 23
        Me.dgDetail.TableStyles.Add(Me.DataGridTableStyle1)
        '
        'DataGridTableStyle1
        '
        Me.DataGridTableStyle1.GridColumnStyles.Add(Me.DataGridTextBoxColumn1)
        Me.DataGridTableStyle1.GridColumnStyles.Add(Me.DataGridTextBoxColumn2)
        Me.DataGridTableStyle1.MappingName = "tbt_GIScan"
        '
        'DataGridTextBoxColumn1
        '
        Me.DataGridTextBoxColumn1.Format = ""
        Me.DataGridTextBoxColumn1.FormatInfo = Nothing
        Me.DataGridTextBoxColumn1.HeaderText = "Pallet ID"
        Me.DataGridTextBoxColumn1.MappingName = "RackID"
        Me.DataGridTextBoxColumn1.Width = 80
        '
        'DataGridTextBoxColumn2
        '
        Me.DataGridTextBoxColumn2.Format = ""
        Me.DataGridTextBoxColumn2.FormatInfo = Nothing
        Me.DataGridTextBoxColumn2.HeaderText = "Scan Date"
        Me.DataGridTextBoxColumn2.MappingName = "ScanDatetimeShow"
        Me.DataGridTextBoxColumn2.Width = 130
        '
        'btnNewProduct
        '
        Me.btnNewProduct.Location = New System.Drawing.Point(147, 246)
        Me.btnNewProduct.Name = "btnNewProduct"
        Me.btnNewProduct.Size = New System.Drawing.Size(85, 20)
        Me.btnNewProduct.TabIndex = 22
        Me.btnNewProduct.Text = "New Product"
        '
        'btnSendToServer
        '
        Me.btnSendToServer.Location = New System.Drawing.Point(6, 270)
        Me.btnSendToServer.Name = "btnSendToServer"
        Me.btnSendToServer.Size = New System.Drawing.Size(72, 20)
        Me.btnSendToServer.TabIndex = 21
        Me.btnSendToServer.Text = "Finish"
        '
        'lblTotalByOrder
        '
        Me.lblTotalByOrder.Font = New System.Drawing.Font("Tahoma", 8.0!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Underline), System.Drawing.FontStyle))
        Me.lblTotalByOrder.Location = New System.Drawing.Point(3, 102)
        Me.lblTotalByOrder.Name = "lblTotalByOrder"
        Me.lblTotalByOrder.Size = New System.Drawing.Size(169, 20)
        Me.lblTotalByOrder.Text = "Total by Order :"
        '
        'Panel1
        '
        Me.Panel1.BackColor = System.Drawing.Color.Gray
        Me.Panel1.Location = New System.Drawing.Point(0, 98)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(240, 2)
        '
        'txtScan
        '
        Me.txtScan.Location = New System.Drawing.Point(82, 74)
        Me.txtScan.Name = "txtScan"
        Me.txtScan.Size = New System.Drawing.Size(147, 21)
        Me.txtScan.TabIndex = 20
        '
        'Label2
        '
        Me.Label2.Font = New System.Drawing.Font("Tahoma", 8.0!, System.Drawing.FontStyle.Bold)
        Me.Label2.Location = New System.Drawing.Point(1, 79)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(85, 20)
        Me.Label2.Text = "Scan Pallet :"
        '
        'lblTitle
        '
        Me.lblTitle.BackColor = System.Drawing.Color.FromArgb(CType(CType(128, Byte), Integer), CType(CType(128, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.lblTitle.Font = New System.Drawing.Font("Tahoma", 10.0!, System.Drawing.FontStyle.Bold)
        Me.lblTitle.ForeColor = System.Drawing.Color.Black
        Me.lblTitle.Location = New System.Drawing.Point(0, 3)
        Me.lblTitle.Name = "lblTitle"
        Me.lblTitle.Size = New System.Drawing.Size(240, 20)
        Me.lblTitle.Text = "Issue Pallet : Scan"
        Me.lblTitle.TextAlign = System.Drawing.ContentAlignment.TopCenter
        '
        'btnDelete
        '
        Me.btnDelete.Location = New System.Drawing.Point(6, 246)
        Me.btnDelete.Name = "btnDelete"
        Me.btnDelete.Size = New System.Drawing.Size(72, 20)
        Me.btnDelete.TabIndex = 28
        Me.btnDelete.Text = "Delete"
        '
        'txtOrder
        '
        Me.txtOrder.BackColor = System.Drawing.Color.Gainsboro
        Me.txtOrder.Location = New System.Drawing.Point(82, 26)
        Me.txtOrder.Name = "txtOrder"
        Me.txtOrder.ReadOnly = True
        Me.txtOrder.Size = New System.Drawing.Size(147, 21)
        Me.txtOrder.TabIndex = 30
        '
        'Label1
        '
        Me.Label1.Font = New System.Drawing.Font("Tahoma", 8.0!, System.Drawing.FontStyle.Bold)
        Me.Label1.Location = New System.Drawing.Point(0, 29)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(85, 20)
        Me.Label1.Text = "Order No. :"
        '
        'lblTotalByProduct
        '
        Me.lblTotalByProduct.Font = New System.Drawing.Font("Tahoma", 8.0!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Underline), System.Drawing.FontStyle))
        Me.lblTotalByProduct.Location = New System.Drawing.Point(4, 119)
        Me.lblTotalByProduct.Name = "lblTotalByProduct"
        Me.lblTotalByProduct.Size = New System.Drawing.Size(168, 20)
        Me.lblTotalByProduct.Text = "Total by Product : "
        '
        'btnNewOrder
        '
        Me.btnNewOrder.Location = New System.Drawing.Point(147, 270)
        Me.btnNewOrder.Name = "btnNewOrder"
        Me.btnNewOrder.Size = New System.Drawing.Size(85, 20)
        Me.btnNewOrder.TabIndex = 36
        Me.btnNewOrder.Text = "New Order"
        '
        'txtProduct
        '
        Me.txtProduct.Location = New System.Drawing.Point(82, 50)
        Me.txtProduct.Name = "txtProduct"
        Me.txtProduct.Size = New System.Drawing.Size(147, 21)
        Me.txtProduct.TabIndex = 38
        '
        'Label4
        '
        Me.Label4.Font = New System.Drawing.Font("Tahoma", 8.0!, System.Drawing.FontStyle.Bold)
        Me.Label4.Location = New System.Drawing.Point(0, 52)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(93, 20)
        Me.Label4.Text = "Scan Product :"
        '
        'frmGIScan
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(96.0!, 96.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi
        Me.AutoScroll = True
        Me.ClientSize = New System.Drawing.Size(240, 294)
        Me.Controls.Add(Me.txtProduct)
        Me.Controls.Add(Me.Label4)
        Me.Controls.Add(Me.dgDetail)
        Me.Controls.Add(Me.btnNewOrder)
        Me.Controls.Add(Me.lblTotalByProduct)
        Me.Controls.Add(Me.txtOrder)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.btnDelete)
        Me.Controls.Add(Me.btnNewProduct)
        Me.Controls.Add(Me.btnSendToServer)
        Me.Controls.Add(Me.lblTotalByOrder)
        Me.Controls.Add(Me.Panel1)
        Me.Controls.Add(Me.txtScan)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.lblTitle)
        Me.Location = New System.Drawing.Point(0, 0)
        Me.Menu = Me.mainMenu1
        Me.Name = "frmGIScan"
        Me.Text = "frmGIScan"
        Me.WindowState = System.Windows.Forms.FormWindowState.Maximized
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents dgDetail As System.Windows.Forms.DataGrid
    Friend WithEvents btnNewProduct As System.Windows.Forms.Button
    Friend WithEvents btnSendToServer As System.Windows.Forms.Button
    Friend WithEvents lblTotalByOrder As System.Windows.Forms.Label
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Friend WithEvents txtScan As System.Windows.Forms.TextBox
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Protected WithEvents lblTitle As System.Windows.Forms.Label
    Friend WithEvents btnDelete As System.Windows.Forms.Button
    Friend WithEvents txtOrder As System.Windows.Forms.TextBox
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents lblTotalByProduct As System.Windows.Forms.Label
    Friend WithEvents btnNewOrder As System.Windows.Forms.Button
    Friend WithEvents txtProduct As System.Windows.Forms.TextBox
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents DataGridTableStyle1 As System.Windows.Forms.DataGridTableStyle
    Friend WithEvents DataGridTextBoxColumn1 As System.Windows.Forms.DataGridTextBoxColumn
    Friend WithEvents DataGridTextBoxColumn2 As System.Windows.Forms.DataGridTextBoxColumn
End Class
