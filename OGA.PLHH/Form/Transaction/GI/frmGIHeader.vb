Imports System.Data

Public Class frmGIHeader



    Private Sub frmGIHeader_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Me.initCtrl()
    End Sub

    Private Sub initCtrl()
        Me.CustomerCode = String.Empty
        CtrlUtil.lockCtrl(Me.txtOrder, False)
        CtrlUtil.lockCtrl(Me.txtCustomer, False)
        CtrlUtil.lockCtrl(Me.txtTruck, False)
        CtrlUtil.lockCtrl(Me.txtDeliveryDate, False)
        Me.txtOrder.Text = String.Empty
        Me.txtCustomer.Text = String.Empty
        Me.txtTruck.Text = String.Empty
        Me.txtDeliveryDate.Text = String.Empty
        Me.txtOrder.Focus()
    End Sub

    Private Sub txtOrder_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txtOrder.KeyPress
        '  e.Handled = ValidateKey.keyNumberic(Me.txtOrder.Text.Trim, e.KeyChar, 30, 0)

        If e.KeyChar = Chr(Keys.Enter) Then
            'If Me.txtOrder.Text.Trim.Length <> 6 Then
            '    OGAMsg.ShowValidate("Wrong format Order data <> 6 digits")
            '    Exit Sub
            'End If
            If CtrlUtil.checkEnterData(Me.txtOrder) = False Then
                Exit Sub
            End If
            If GlobalVariable.ConnectionStatus = "Online" Then
                Me.getOrderDataFromServer(Me.txtOrder.Text.Trim)
            Else
                Me.txtCustomer.Focus()
            End If
        End If
    End Sub

    Private CustomerCode As String

#Region "getOrderDataFromServer"
    Private Sub getOrderDataFromServer(ByVal OrderNo As String)
        Try
            Cursor.Current = Cursors.WaitCursor

            Dim wsGi As WSGI.GIService
            wsGi = InstanceService.GetWSGIInstance

            Dim requestEnt As New WSGI.GIEntity

            requestEnt.OrderNo = Me.txtOrder.Text.Trim


            Dim resultEnt As New WSGI.GIEntity

            resultEnt = wsGi.GetOrderData(requestEnt)

            If resultEnt.IsComplete = True Then
                GlobalVariable.dsOrder = New DataSet
                GlobalVariable.dsOrder = resultEnt.ListGetData

                If GlobalVariable.dsOrder.Tables(0).Rows.Count = 0 Then
                    OGAMsg.ShowResult("Not found" & vbCrLf & "Order : " & OrderNo & vbCrLf & "on Server !")
                Else
                    Me.txtCustomer.Text = GlobalVariable.dsOrder.Tables(0).Rows(0).Item("CNME").ToString.Trim
                    Me.txtTruck.Text = GlobalVariable.dsOrder.Tables(0).Rows(0).Item("TRUCK_LICENSE").ToString.Trim
                    Me.txtDeliveryDate.Text = GlobalVariable.dsOrder.Tables(0).Rows(0).Item("LRDTE").ToString.Trim
                    If Me.txtDeliveryDate.Text.Trim <> String.Empty Then
                        If Me.txtDeliveryDate.Text.Trim.Length = 8 Then
                            Dim tmp As String
                            tmp = txtDeliveryDate.Text.Trim
                            Me.txtDeliveryDate.Text = tmp.Substring(6, 2) & "/" & tmp.Substring(4, 2) & "/" & tmp.Substring(0, 4)
                        End If
                    End If
                    Me.CustomerCode = GlobalVariable.dsOrder.Tables(0).Rows(0).Item("LCUST").ToString.Trim
                    CtrlUtil.lockCtrl(Me.txtOrder, True)
                    CtrlUtil.lockCtrl(Me.txtCustomer, True)
                    CtrlUtil.lockCtrl(Me.txtTruck, True)
                    CtrlUtil.lockCtrl(Me.txtDeliveryDate, True)
                End If
            Else
                OGAMsg.ShowErrorFromWS(resultEnt.ErrorMessage)
            End If

        Catch ex As System.Net.WebException
            OGAMsg.ShowErrorConnectWebService(ex)
        Catch ex As Exception
            OGAMsg.ShowErrorTryCatch(ex)
        Finally
            Cursor.Current = Cursors.Default
            Me.txtCustomer.Focus()
        End Try
    End Sub
#End Region

    Private Sub btnOK_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnOK.Click
        If Me.checkDataBefore = False Then
            Exit Sub
        End If
        Dim f As New frmGIScan
        f.txtOrder.Text = Me.txtOrder.Text.Trim
        f.OrderNo = Me.txtOrder.Text.Trim

        If Me.txtCustomer.ReadOnly = True Then '��ʹ֧�Ҩҡ Server ���Ҩ�� Text ��

            f.CustomerCode = Me.CustomerCode
            f.isOnline = True
        Else
            f.CustomerCode = Me.txtCustomer.Text.Trim
        End If
        f.ShowDialog()
        Me.initCtrl()
    End Sub

#Region "Private Function checkDataBefore() As Boolean"
    Private Function checkDataBefore() As Boolean
        If CtrlUtil.checkEnterData(Me.txtOrder, "Please enter Order !") = False Then
            Return False
        End If

        If CtrlUtil.checkEnterData(Me.txtCustomer, "Please enter Customer code !") = False Then
            Return False
        End If

        If IsNumeric(Me.txtOrder.Text.Trim) = False Then
            OGAMsg.ShowValidate("Wrong data !" & vbCrLf & "Order not numeric" & vbCrLf & "Please try again !")
            Me.txtOrder.Text = String.Empty
            Me.txtOrder.Focus()
            Return False
        End If

        If Me.txtCustomer.ReadOnly = False Then '��� User �кآ������ͧ��ͧ��Ǩ�ͺ����繵���Ţ�������
            If IsNumeric(Me.txtCustomer.Text.Trim) = False Then
                OGAMsg.ShowValidate("Wrong data !" & vbCrLf & "Customer code not numeric" & vbCrLf & "Please try again !")
                Me.txtCustomer.Text = String.Empty
                Me.txtCustomer.Focus()
                Return False
            End If
        End If
        

        Return True
    End Function
#End Region

    Private Sub btnReset_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnReset.Click
        Me.initCtrl()
    End Sub

    Private Sub btnBack_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnBack.Click
        Me.Close()
    End Sub


    Private Sub txtCustomer_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txtCustomer.KeyPress
        'e.Handled = ValidateKey.keyNumberic(Me.txtCustomer.Text, e.KeyChar, 30, 0)

    End Sub
End Class