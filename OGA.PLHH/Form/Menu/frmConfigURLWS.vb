Public Class frmConfigURLWS

    Private Sub frmConfigURLWS_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Me.txtCurrentURL.Text = GlobalVariable.WSOtherURL

        Me.txtNewURL.Focus()
    End Sub

    Private Sub btnExit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnExit.Click
        Me.DialogResult = Windows.Forms.DialogResult.No
    End Sub

    Private Sub btnTest_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnTest.Click
        If CtrlUtil.checkEnterData(Me.txtNewURL) = False Then
            Exit Sub
        End If
        Me.checkServer(Me.txtNewURL.Text.Trim)
    End Sub

    Private Sub btnSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSave.Click
        If CtrlUtil.checkEnterData(Me.txtNewURL) = False Then
            Exit Sub
        End If

        If Me.checkServer(Me.txtNewURL.Text.Trim) = True Then
            Try
                Dim dal As New ConfigFileDAL
                dal.writeIPServer(Me.txtNewURL.Text.Trim)
                OGAMsg.ShowResult("Save new URL Web Service completed !")
                Application.Exit()
            Catch ex As Exception
                OGAMsg.ShowErrorTryCatch("Write new URL Web Service error !", ex)
            End Try
        End If
    End Sub

    Private Function checkServer(ByVal newURL As String) As Boolean
        Try
            Cursor.Current = Cursors.WaitCursor
            If InstanceService.IsCanConnentOnline(newURL) = False Then
                GlobalVariable.ConnectionStatus = "Offline"
                OGAMsg.ShowResult("Can not connect to" & vbCrLf & "Web Service URL:" & vbCrLf & newURL)
            Else
                OGAMsg.ShowResult("Connect to Web Service URL:" & vbCrLf & newURL & vbCrLf & "Successful !")
                GlobalVariable.ConnectionStatus = "Online"
                Return True
            End If
        Catch ex As System.Net.WebException
            OGAMsg.ShowErrorConnectWebService(ex)
        Catch ex As Exception
            OGAMsg.ShowErrorTryCatch(ex)
        Finally
            Cursor.Current = Cursors.Default
        End Try
    End Function




End Class