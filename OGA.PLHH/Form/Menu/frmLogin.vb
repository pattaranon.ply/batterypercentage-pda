Imports System.Data
Imports System.IO
Imports System.Xml

Public Class frmLogin

    Private Sub frmLogin_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Me.showVersion()
        Me.initCtrl()

        'Me.txtUsername.Text = "admin"
        'Me.txtPassword.Text = "admin"
        'Me.loginToWMS()
    End Sub

    Private Sub showVersion()
        GlobalVariable.CurrentVersion = System.Reflection.Assembly.GetExecutingAssembly().GetName().Version.ToString() '�֧�����������ش�ҡ Assembly infomation
        Me.lblVersion.Text = "Version : v." & GlobalVariable.CurrentVersion
    End Sub

    Private Sub initCtrl()
        Me.txtUsername.Text = String.Empty
        Me.txtPassword.Text = String.Empty
        Me.txtUsername.Focus()
    End Sub


    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Application.Exit()
    End Sub

    Private Sub btnUserDownload_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnUserDownload.Click
        Dim f As New frmUserDownload
        f.ShowDialog()
    End Sub

    Private Sub btnLogin_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnLogin.Click
        Me.loginToWMS()
    End Sub

    Private Sub txtUserName_KeyPress(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txtUsername.KeyPress
        If e.KeyChar = Chr(13) Then
            If txtPassword.Text = String.Empty Then
                txtPassword.Focus()
            Else
                Me.loginToWMS()
            End If
        End If
    End Sub

    Private Sub txtPassword_KeyPress(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txtPassword.KeyPress
        If e.KeyChar = Chr(13) Then
            Me.loginToWMS()
        End If
    End Sub

    Private Sub txtPassword_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtPassword.TextChanged
        Me.changeToPasswordChar()
    End Sub

    'Support for scan barcode password
    Private Sub changeToPasswordChar()
        Dim passChar = Convert.ToChar("*")
        If Me.txtPassword.Text.Trim <> String.Empty Then
            Me.txtPassword.PasswordChar = passChar
        Else
            Me.txtPassword.PasswordChar = New Char()
        End If
    End Sub

#Region "Private Sub loginToWMS()"

    Private Function validateInput() As Boolean
        If CtrlUtil.checkEnterData(Me.txtUsername) = False Then
            Return False
        End If
        If CtrlUtil.checkEnterData(Me.txtPassword) = False Then
            Return False
        End If
        Return True
    End Function

    Private Sub loginToWMS()
        If Me.validateInput = False Then
            Exit Sub
        End If

        If FileUtil.checkUserFile = False Then
            Exit Sub
        End If

        Try
            Cursor.Current = Cursors.WaitCursor

            Dim userName, passWord As String

            userName = Me.txtUsername.Text.Trim
            passWord = Me.txtPassword.Text.Trim
            Dim ds As New DataSet
            ds.ReadXml("tbt_UserPDA.xml")
            Dim dv As New DataView
            dv = ds.Tables(0).DefaultView
            dv.RowFilter = "UserName = '" & userName & "' "
            If dv.Count = 0 Then
                WMSMsg.ShowValidate("���ͼ����ҹ�Դ" & vbCrLf & "��سҵ�Ǩ�ͺ�ա���� !")
                Me.txtUsername.Text = String.Empty
                Me.txtPassword.Text = String.Empty
                Me.txtUsername.Focus()
                Exit Sub
            End If
            Dim tmp As String
            Dim isFound As Boolean = False
            For i As Integer = 0 To dv.Count - 1
                tmp = dv.Item(i).Item("KeyID").ToString
                If tmp.Trim <> "" Then
                    Dim length As Integer
                    length = CInt(dv.Item(i).Item("KeyID").ToString.Trim.Substring(0, 1))
                    Dim pass As String
                    pass = dv.Item(i).Item("KeyID").ToString.Trim.Substring(10, length)
                    If pass = Me.txtPassword.Text.Trim Then
                        isFound = True
                        GlobalVariable.UserFullName = dv.Item(i).Item("UserFullName").ToString
                        Exit For
                    End If
                End If
            Next i

            If isFound = False Then
                WMSMsg.ShowValidate("���ͼ����ҹ ���� ���ʼ�ҹ�Դ" & vbCrLf & "��سҵ�Ǩ�ͺ�ա���� !")
                Me.txtUsername.Text = String.Empty
                Me.txtPassword.Text = String.Empty
                Me.txtUsername.Focus()
                Exit Sub
            End If
            Cursor.Current = Cursors.Default
            Dim f As New frmMenuBarcode
            f.ShowDialog()
            Me.txtUsername.Text = String.Empty
            Me.txtPassword.Text = String.Empty
            Me.txtUsername.Focus()


        Catch ex As System.Net.WebException
            WMSMsg.ShowErrorConnectWebService(ex)
        Catch ex As Exception
            WMSMsg.ShowErrorTryCatch("��� Login �����ҹ �Դ��Ҵ !", ex)
        Finally
            Cursor.Current = Cursors.Default
        End Try
    End Sub
#End Region

End Class