Imports System.Data

Public Class frmMenu

    Private Sub frmMenu_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Me.getXMLConfigValue()
        'Me.getDateFromServer()
        Me.lblDate.Text = "Date : " & Format(Date.Now, "yyyy-MM-dd")
        Me.initConnection()
        Me.lblIPServer.Text = "Server IP : " & GlobalVariable.IPServer
        'Me.setTime()
        'Me.showIP()
    End Sub

    'Private Sub showIP()
    '    Try
    '        Dim myAccessPoint As OpenNETCF.Net.AccessPoint
    '        Dim MyNWLoc As String = myAccessPoint.
    '        Dim MyNWdb As String = myAccessPoint.SignalStrength.ToString
    '        MsgBox(MyNWLoc & "  " & MyNWdb)
    '    Catch ex As Exception
    '        OGAMsg.ShowErrorTryCatch(ex)
    '    End Try
    'End Sub

    Private Sub setTime()
        Dim obj As New APISetTime
        obj.SetTimeZone(-420)
        Dim d As New Date
        d = CDate("2/3/2011 7:15 PM")
        obj.UpdateSystemTime(d)
    End Sub

    Private Sub getXMLConfigValue()
        Try
            Cursor.Current = Cursors.WaitCursor
            Dim obj As New ConfigFileDAL
            obj.getXMLConfigValue()

        Catch ex As Exception
            OGAMsg.ShowErrorTryCatch(ex)
        Finally
            Cursor.Current = Cursors.Default
        End Try
    End Sub

    Private isOnline As Boolean = False

    Private Sub initConnection()
        Try
            Cursor.Current = Cursors.WaitCursor

            GlobalVariable.CurrentVersion = System.Reflection.Assembly.GetExecutingAssembly().GetName().Version.ToString() '�֧�����������ش�ҡ Assembly infomation
            Me.lblVersion.Text = "Version : v." & GlobalVariable.CurrentVersion

            'Check �������ö�Դ��� Web Service ��������� �¡�õԴ��� OtherService
            If InstanceService.IsCanConnentOnline = False Then
                GlobalVariable.ConnectionStatus = "Offline"
                Me.lblOnline.Text = "Status : " & GlobalVariable.ConnectionStatus
                Exit Sub
            Else
                GlobalVariable.ConnectionStatus = "Online"
                Me.lblOnline.Text = "Status : " & GlobalVariable.ConnectionStatus
            End If

            If Me.InitialWebServiceValue() = True Then
                Me.isOnline = True
                '�Ѿഷ����� HH

                Dim wsOther As WSOther.OtherService
                wsOther = InstanceService.GetWSOtherInstance
                Dim info As String() = wsOther.GetUpdateHHInfo()
                Dim programHHVersion As String = info(0) '������� HH
                Dim programHHInstallURL As String = info(1) 'URL .cab ���������Ѻ installHH

                '��Ǩ�ͺ�����������ش
                If programHHVersion <> GlobalVariable.CurrentVersion Then
                    OGAMsg.ShowValidate("Please update program!" & ControlChars.NewLine & _
                                                   "Current version : v." & GlobalVariable.CurrentVersion & ControlChars.NewLine & _
                                                   "New version : v." & programHHVersion)
                    System.Diagnostics.Process.Start("\Windows\iexplore.exe", programHHInstallURL)   '�Դ IE �������¡ URL ��� install HH
                    Me.DialogResult = Windows.Forms.DialogResult.Cancel
                End If

            End If
        Catch ex As System.Net.WebException
            'OGAMsg.ShowErrorConnectWebService(ex)
            Me.lblOnline.Text = "Status : Offline"
            Me.lblVersion.Text = "Version : v." & GlobalVariable.CurrentVersion
            Me.DialogResult = Windows.Forms.DialogResult.Cancel

        Catch ex As Exception
            OGAMsg.ShowErrorTryCatch(ex)
            Me.lblOnline.Text = String.Format(Me.lblOnline.Text, "Offline")
            Me.DialogResult = Windows.Forms.DialogResult.Cancel
        Finally
            Cursor.Current = Cursors.Default
        End Try
    End Sub

    Public Function InitialWebServiceValue() As Boolean
        Try
            Dim wsOther As WSOther.OtherService
            wsOther = InstanceService.GetWSOtherInstance
            Dim ds As DataSet = wsOther.GetAllURLWSAndTimeOutAndVersionForHH
            InstanceService.SetWSInitialValue(ds)

            Dim obj As New APISetTime
            'obj.SetTimeZone(-420)
            Dim d As New DateTime
            d = CDate(ds.Tables("ServerDate").Rows(0).Item("ServerDate").ToString)
            obj.UpdateSystemTime(d)
            Me.lblDate.Text = "Date : " & Format(Date.Now, "yyyy-MM-dd")

            'Dim dateNow As String
            'dateNow = ds.Tables("TableDateNow").Rows(0).Item("DateNow")
            'dateNow = dateNow.Substring(0, 4) & "-" & dateNow.Substring(4, 2) & "-" & dateNow.Substring(6, 2)
            'Me.lblDate.Text = dateNow
            Return True
        Catch ex As System.Net.WebException
            Throw
        Catch ex As Exception
            Throw
        End Try
    End Function

    Private Sub showVersion()
        Me.lblVersion.Text = String.Format(Me.lblVersion.Text, "1.0.1.0")
    End Sub

    Private Sub getDateFromServer()
        Try
            Cursor.Current = Cursors.WaitCursor

        Catch ex As System.Net.WebException
            OGAMsg.ShowErrorConnectWebService(ex)
        Catch ex As Exception
            OGAMsg.ShowErrorTryCatch(ex)
        Finally
            Cursor.Current = Cursors.Default
        End Try
    End Sub

    Private Sub showConnectionStatus()
        Me.lblOnline.Text = "Status : " & GlobalVariable.ConnectionStatus
    End Sub

    Private Sub btnReceive_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnReceive.Click
        Dim f As New frmGRScan
        GlobalVariable.dsGR = New DataSet
        f.ShowDialog()
        If GlobalVariable.ConnectionStatus = "Offline" Then
            Me.initConnection()
        End If
        If Not GlobalVariable.dsGR Is Nothing Then
            GlobalVariable.dsGR.Dispose()
        End If
        Me.lblDate.Text = "Date : " & Format(Date.Now, "yyyy-MM-dd")
    End Sub

    Private Sub btnIssue_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnIssue.Click
        If Not GlobalVariable.dsGI Is Nothing Then
            GlobalVariable.dsGI.Dispose()
        End If

        Dim f As New frmGIHeader
        GlobalVariable.dsGI = New DataSet
        f.ShowDialog()
        If GlobalVariable.ConnectionStatus = "Offline" Then
            Me.initConnection()
        End If


        If Not GlobalVariable.dsGI Is Nothing Then
            GlobalVariable.dsGI.Dispose()
        End If

        Me.lblDate.Text = "Date : " & Format(Date.Now, "yyyy-MM-dd")
    End Sub

    Private Sub btnExit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnExit.Click
        Dim f As New frmLogOut
        f.ShowDialog()
    End Sub


    Private Sub btnUpload_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnUpload.Click
        If GlobalVariable.ConnectionStatus = "Offline" Then
            Me.initConnection()
        End If

        If GlobalVariable.ConnectionStatus = "Offline" Then '��ѧ�ҡ�ͧ Connect ����ѧ Offline �ա
            OGAMsg.ShowValidate("Can not upload data to Server !" & vbCrLf & "It is Offline mode.")
        Else
            If OGAMsg.ShowConfirm("Are you sure" & vbCrLf & "Upload data to Server !") = Windows.Forms.DialogResult.Yes Then
                Me.isHavePalletData = False 'initial Value

                If Me.confirmDataReceiveToServer() = True Then
                    If Me.confirmDataIssueToServer = True Then
                        If Me.isHavePalletData = True Then
                            OGAMsg.ShowResult("Upload data completed !")
                        Else
                            OGAMsg.ShowResult("No data to upload !")
                        End If

                    End If
                End If
            End If
           
        End If
    End Sub

    Dim isHavePalletData As Boolean

#Region "confirmDataReceiveToServer"
    Private Function confirmDataReceiveToServer() As Boolean
        Try
            Cursor.Current = Cursors.WaitCursor

            GlobalVariable.dsGR = New DataSet
            Dim dal As New GRDAL
            GlobalVariable.dsGR = dal.getAllData

            If GlobalVariable.dsGR.Tables("tbt_GRScan").Rows.Count = 0 Then
                'OGAMsg.ShowValidate("No Receive Pallet Data !")
                Cursor.Current = Cursors.Default
                Return True
            End If

            Me.isHavePalletData = True '�ʴ�����բ����ž��ŷ����� Server

            Dim wsGR As WSGR.GRService
            wsGR = InstanceService.GetWSGRInstance

            Dim requestEnt As New WSGR.GREntity
            requestEnt.ListConfirmData = GlobalVariable.dsGR

            Dim resultEnt As New WSGR.GREntity

            resultEnt = wsGR.WriteScanDataToServer(requestEnt)

            If resultEnt.IsComplete = True Then
                dal.deleteAllData()
                Cursor.Current = Cursors.Default
                Return True
            Else
                OGAMsg.ShowErrorFromWS(resultEnt.ErrorMessage)
            End If

        Catch ex As System.Net.WebException
            OGAMsg.ShowErrorConnectWebService(ex)
        Catch ex As Exception
            OGAMsg.ShowErrorTryCatch(ex)
        Finally
            If Not GlobalVariable.dsGR Is Nothing Then
                GlobalVariable.dsGR.Dispose()
            End If
            Cursor.Current = Cursors.Default
        End Try
    End Function
#End Region


#Region "confirmDataIssueToServer"
    Private Function confirmDataIssueToServer() As Boolean
        Try
            Cursor.Current = Cursors.WaitCursor

            GlobalVariable.dsGI = New DataSet
            Dim dal As New GIDAL
            GlobalVariable.dsGI = dal.getAllData

            If GlobalVariable.dsGI.Tables("tbt_GIScan").Rows.Count = 0 Then
                'OGAMsg.ShowValidate("No Issue Pallet Data !")
                Cursor.Current = Cursors.Default
                Return True
            End If

            Me.isHavePalletData = True '�ʴ�����բ����ž��ŷ����� Server

            Dim wsGI As WSGI.GIService
            wsGI = InstanceService.GetWSGIInstance

            Dim requestEnt As New WSGI.GIEntity
            requestEnt.ListConfirmData = GlobalVariable.dsGI

            Dim resultEnt As New WSGI.GIEntity

            resultEnt = wsGI.WriteScanDataToServer(requestEnt)

            If resultEnt.IsComplete = True Then
                dal.deleteAllData()
                Cursor.Current = Cursors.Default
                Return True
            Else
                OGAMsg.ShowErrorFromWS(resultEnt.ErrorMessage)
            End If

        Catch ex As System.Net.WebException
            OGAMsg.ShowErrorConnectWebService(ex)
        Catch ex As Exception
            OGAMsg.ShowErrorTryCatch(ex)
        Finally
            If Not GlobalVariable.dsGI Is Nothing Then
                GlobalVariable.dsGI.Dispose()
            End If
            Cursor.Current = Cursors.Default
        End Try
    End Function
#End Region

    Private Sub Timer1_Tick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Timer1.Tick
        Try
            batt.UpdateBatteryLife()
            batt.Refresh()
        Catch ex As Exception

        End Try
    End Sub

  
End Class
