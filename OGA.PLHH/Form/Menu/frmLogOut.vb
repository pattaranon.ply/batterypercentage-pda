Public Class frmLogOut

 
    Private Sub frmLogOut_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Me.initCtrl()
    End Sub

    Private Sub initCtrl()
        Me.txtPassword.Text = String.Empty
        Me.txtPassword.Focus()
        Me.lblDate.Text = Format(Date.Now, "yyyy-MM-dd")
    End Sub

    Private Sub btnOK_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnOK.Click
        If Me.checkPassword() = True Then
            Application.Exit()
        End If
    End Sub

    Private Function checkPassword() As Boolean
        Try
            If CtrlUtil.checkEnterData(Me.txtPassword, "Please enter password !") = False Then
                Return False
            End If
            Dim tmp() As String
            tmp = Me.lblDate.Text.Trim.Split("-")

            If tmp.Length <> 0 Then
                Dim password As Integer
                password = CInt(tmp(0)) - CInt(tmp(1)) - CInt(tmp(2))

                If Me.txtPassword.Text.Trim = password.ToString Then
                    Return True
                Else
                    OGAMsg.ShowValidate("Invalid Password !" & vbCrLf & "Try again")
                    Me.txtPassword.Text = String.Empty
                    Me.txtPassword.Focus()
                    Return False
                End If
            End If

        Catch ex As Exception
            OGAMsg.ShowErrorTryCatch(ex)
        End Try
    End Function

    Private Sub btnExit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnExit.Click
        Me.Close()
    End Sub

    Private Sub txtPassword_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txtPassword.KeyPress
        If e.KeyChar = Chr(Keys.Enter) Then
            If Me.checkPassword() = True Then
                 Application.Exit()
            End If
        End If
    End Sub


    Private Sub btnConfig_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnConfig.Click
        'If Me.checkPassword() = True Then
        '    Dim f As New frmConfigIPServer
        '    f.ShowDialog()
        'End If
        If Me.checkPassword() = True Then
            Dim f As New frmConfigURLWS
            f.ShowDialog()
        End If
    End Sub
End Class