Public Class frmConfigIPServer

   
    Private Sub frmConfigIPServer_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Me.showCurrentIP()
        Me.txtNewIP.Focus()
    End Sub

    Private Sub showCurrentIP()
        Try
            Me.txtCurrentIP.Text = GlobalVariable.IPServer
        Catch ex As Exception
            OGAMsg.ShowErrorTryCatch(ex)
        End Try
    End Sub

    Private Sub getXMLConfigValue()
        Try
            Cursor.Current = Cursors.WaitCursor
            Dim obj As New ConfigFileDAL
            obj.getXMLConfigValue()

        Catch ex As Exception
            OGAMsg.ShowErrorTryCatch(ex)
        Finally
            Cursor.Current = Cursors.Default
        End Try
    End Sub


    Private Sub btnTest_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnTest.Click
        If CtrlUtil.checkEnterData(Me.txtNewIP) = False Then
            Exit Sub
        End If
        Me.checkServer(Me.txtNewIP.Text.Trim)
    End Sub

    Private Function checkServer(ByVal newIP As String) As Boolean
        Try
            Cursor.Current = Cursors.WaitCursor
            If InstanceService.IsCanConnentOnline(newip) = False Then
                GlobalVariable.ConnectionStatus = "Offline"
                OGAMsg.ShowResult("Can not connect to server !")
            Else
                GlobalVariable.ConnectionStatus = "Online"
                Return True
            End If
        Catch ex As System.Net.WebException
            OGAMsg.ShowErrorConnectWebService(ex)
        Catch ex As Exception
            OGAMsg.ShowErrorTryCatch(ex)
        Finally
            Cursor.Current = Cursors.Default
        End Try
    End Function

    Private Sub btnSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSave.Click
        If CtrlUtil.checkEnterData(Me.txtNewIP) = False Then
            Exit Sub
        End If
        If Me.checkServer(Me.txtNewIP.Text.Trim) = True Then
            Try
                Dim dal As New ConfigFileDAL
                dal.writeIPServer(Me.txtNewIP.Text.Trim)
                OGAMsg.ShowResult("Save new IP Server completed !")
                Application.Exit()
            Catch ex As Exception
                OGAMsg.ShowErrorTryCatch("Write new IP Server error !", ex)
            End Try
        End If
    End Sub

    Private Sub btnExit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnExit.Click
        Me.Close()
    End Sub
End Class