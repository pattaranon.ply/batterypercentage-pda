<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Public Class frmMenu
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmMenu))
        Me.Label1 = New System.Windows.Forms.Label
        Me.Label2 = New System.Windows.Forms.Label
        Me.btnReceive = New System.Windows.Forms.Button
        Me.btnIssue = New System.Windows.Forms.Button
        Me.btnExit = New System.Windows.Forms.Button
        Me.lblVersion = New System.Windows.Forms.Label
        Me.lblDate = New System.Windows.Forms.Label
        Me.lblOnline = New System.Windows.Forms.Label
        Me.lblIPServer = New System.Windows.Forms.Label
        Me.btnUpload = New System.Windows.Forms.Button
        Me.UcSignal1 = New OGA.PLHH.ucSignal
        Me.batt = New OpenNETCF.Windows.Forms.BatteryLife
        Me.pic = New System.Windows.Forms.PictureBox
        Me.Timer1 = New System.Windows.Forms.Timer
        Me.SuspendLayout()
        '
        'Label1
        '
        Me.Label1.BackColor = System.Drawing.Color.FromArgb(CType(CType(128, Byte), Integer), CType(CType(128, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.Label1.Font = New System.Drawing.Font("Tahoma", 11.0!, System.Drawing.FontStyle.Bold)
        Me.Label1.ForeColor = System.Drawing.Color.Black
        Me.Label1.Location = New System.Drawing.Point(0, 3)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(240, 20)
        Me.Label1.Text = "Pallet Control."
        Me.Label1.TextAlign = System.Drawing.ContentAlignment.TopCenter
        '
        'Label2
        '
        Me.Label2.BackColor = System.Drawing.Color.FromArgb(CType(CType(128, Byte), Integer), CType(CType(128, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.Label2.Font = New System.Drawing.Font("Tahoma", 11.0!, System.Drawing.FontStyle.Bold)
        Me.Label2.ForeColor = System.Drawing.Color.Black
        Me.Label2.Location = New System.Drawing.Point(0, 28)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(240, 20)
        Me.Label2.Text = "Main Menu"
        Me.Label2.TextAlign = System.Drawing.ContentAlignment.TopCenter
        '
        'btnReceive
        '
        Me.btnReceive.Location = New System.Drawing.Point(11, 55)
        Me.btnReceive.Name = "btnReceive"
        Me.btnReceive.Size = New System.Drawing.Size(216, 37)
        Me.btnReceive.TabIndex = 12
        Me.btnReceive.Tag = ""
        Me.btnReceive.Text = "Receive Pallet"
        '
        'btnIssue
        '
        Me.btnIssue.Location = New System.Drawing.Point(11, 100)
        Me.btnIssue.Name = "btnIssue"
        Me.btnIssue.Size = New System.Drawing.Size(216, 37)
        Me.btnIssue.TabIndex = 12
        Me.btnIssue.Tag = ""
        Me.btnIssue.Text = "Issue Pallet"
        '
        'btnExit
        '
        Me.btnExit.Location = New System.Drawing.Point(11, 201)
        Me.btnExit.Name = "btnExit"
        Me.btnExit.Size = New System.Drawing.Size(216, 37)
        Me.btnExit.TabIndex = 13
        Me.btnExit.Tag = ""
        Me.btnExit.Text = "Exit"
        '
        'lblVersion
        '
        Me.lblVersion.ForeColor = System.Drawing.Color.Black
        Me.lblVersion.Location = New System.Drawing.Point(114, 262)
        Me.lblVersion.Name = "lblVersion"
        Me.lblVersion.Size = New System.Drawing.Size(114, 20)
        Me.lblVersion.Text = "Version : {0}"
        Me.lblVersion.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'lblDate
        '
        Me.lblDate.ForeColor = System.Drawing.Color.Black
        Me.lblDate.Location = New System.Drawing.Point(79, 280)
        Me.lblDate.Name = "lblDate"
        Me.lblDate.Size = New System.Drawing.Size(148, 20)
        Me.lblDate.Text = "Date : {0}"
        Me.lblDate.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'lblOnline
        '
        Me.lblOnline.BackColor = System.Drawing.Color.FromArgb(CType(CType(128, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(128, Byte), Integer))
        Me.lblOnline.Font = New System.Drawing.Font("Tahoma", 9.0!, System.Drawing.FontStyle.Bold)
        Me.lblOnline.ForeColor = System.Drawing.Color.Black
        Me.lblOnline.Location = New System.Drawing.Point(114, 243)
        Me.lblOnline.Name = "lblOnline"
        Me.lblOnline.Size = New System.Drawing.Size(114, 20)
        Me.lblOnline.Text = "Status : {0}"
        Me.lblOnline.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'lblIPServer
        '
        Me.lblIPServer.ForeColor = System.Drawing.Color.Black
        Me.lblIPServer.Location = New System.Drawing.Point(62, 298)
        Me.lblIPServer.Name = "lblIPServer"
        Me.lblIPServer.Size = New System.Drawing.Size(166, 20)
        Me.lblIPServer.Text = "Server IP : {0}"
        Me.lblIPServer.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'btnUpload
        '
        Me.btnUpload.Location = New System.Drawing.Point(11, 146)
        Me.btnUpload.Name = "btnUpload"
        Me.btnUpload.Size = New System.Drawing.Size(216, 40)
        Me.btnUpload.TabIndex = 23
        Me.btnUpload.Text = "Upload"
        '
        'UcSignal1
        '
        Me.UcSignal1.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular)
        Me.UcSignal1.Location = New System.Drawing.Point(11, 262)
        Me.UcSignal1.Name = "UcSignal1"
        Me.UcSignal1.Size = New System.Drawing.Size(35, 20)
        Me.UcSignal1.TabIndex = 16
        '
        'batt
        '
        Me.batt.Font = New System.Drawing.Font("Tahoma", 8.0!, System.Drawing.FontStyle.Regular)
        Me.batt.Location = New System.Drawing.Point(13, 300)
        Me.batt.Name = "batt"
        Me.batt.PercentageBarColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.batt.Size = New System.Drawing.Size(39, 13)
        Me.batt.TabIndex = 30
        Me.batt.TabStop = False
        '
        'pic
        '
        Me.pic.Image = CType(resources.GetObject("pic.Image"), System.Drawing.Image)
        Me.pic.Location = New System.Drawing.Point(11, 298)
        Me.pic.Name = "pic"
        Me.pic.Size = New System.Drawing.Size(49, 19)
        '
        'Timer1
        '
        Me.Timer1.Enabled = True
        Me.Timer1.Interval = 5000
        '
        'frmMenu
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(96.0!, 96.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi
        Me.AutoScroll = True
        Me.ClientSize = New System.Drawing.Size(240, 320)
        Me.Controls.Add(Me.batt)
        Me.Controls.Add(Me.btnUpload)
        Me.Controls.Add(Me.UcSignal1)
        Me.Controls.Add(Me.lblIPServer)
        Me.Controls.Add(Me.lblOnline)
        Me.Controls.Add(Me.lblDate)
        Me.Controls.Add(Me.lblVersion)
        Me.Controls.Add(Me.btnExit)
        Me.Controls.Add(Me.btnIssue)
        Me.Controls.Add(Me.btnReceive)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.pic)
        Me.Location = New System.Drawing.Point(0, 0)
        Me.Name = "frmMenu"
        Me.Text = "Form1"
        Me.WindowState = System.Windows.Forms.FormWindowState.Maximized
        Me.ResumeLayout(False)

    End Sub
    Protected WithEvents Label1 As System.Windows.Forms.Label
    Protected WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents btnReceive As System.Windows.Forms.Button
    Friend WithEvents btnIssue As System.Windows.Forms.Button
    Friend WithEvents btnExit As System.Windows.Forms.Button
    Friend WithEvents lblVersion As System.Windows.Forms.Label
    Friend WithEvents lblDate As System.Windows.Forms.Label
    Friend WithEvents lblOnline As System.Windows.Forms.Label
    Friend WithEvents lblIPServer As System.Windows.Forms.Label
    Friend WithEvents UcSignal1 As OGA.PLHH.ucSignal
    Friend WithEvents btnUpload As System.Windows.Forms.Button
    Friend WithEvents batt As OpenNETCF.Windows.Forms.BatteryLife
    Friend WithEvents pic As System.Windows.Forms.PictureBox
    Friend WithEvents Timer1 As System.Windows.Forms.Timer

End Class
