Imports System.Data
Imports System.IO
Imports System.Xml


Public Class frmUserDownload

    Private Sub frmUserDownload_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Me.showDataGrid()
    End Sub

    Private Sub showDataGrid()
        If FileUtil.checkUserFile = False Then
            Exit Sub
        End If
        Try
            Cursor.Current = Cursors.WaitCursor
            Dim ds As New DataSet
            ds.ReadXml("tbt_UserPDA.xml")
            Me.dgvDetail.DataSource = ds.Tables("TableDetail")
        Catch ex As Exception
            OGAMsg.ShowErrorTryCatch("����ʴ������ż����ҹ �Դ��Ҵ !", ex)
        Finally
            Cursor.Current = Cursors.Default
        End Try
    End Sub

    Private Sub btnExit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnExit.Click
        Me.DialogResult = Windows.Forms.DialogResult.Cancel
    End Sub
End Class