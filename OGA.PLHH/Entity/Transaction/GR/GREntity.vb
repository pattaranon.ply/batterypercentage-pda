Public Class GREntity


    Private _RackID As String
    Private _ScanDatetime As Date
    Private _ScanDatetimeShow As String

    Public Property ScanDatetimeShow() As String
        Get
            Return _ScanDatetimeShow
        End Get
        Set(ByVal value As String)
            _ScanDatetimeShow = value
        End Set
    End Property

    Public Property ScanDatetime() As Date
        Get
            Return _ScanDatetime
        End Get
        Set(ByVal value As Date)
            _ScanDatetime = value
        End Set
    End Property

    Public Property RackID() As String
        Get
            Return _RackID
        End Get
        Set(ByVal value As String)
            _RackID = value
        End Set
    End Property

End Class
