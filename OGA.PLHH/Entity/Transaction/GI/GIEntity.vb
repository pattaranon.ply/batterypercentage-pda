Public Class GIEntity

    Private _RackID As String
    Private _ScanDatetime As Date
    Private _ScanDatetimeShow As String


    Private _OrderNo As String
    Private _ProductCode As String
    Private _CustomerCode As String

    Public Property CustomerCode() As String
        Get
            Return _CustomerCode
        End Get
        Set(ByVal value As String)
            _CustomerCode = value
        End Set
    End Property

    Public Property ProductCode() As String
        Get
            Return _ProductCode
        End Get
        Set(ByVal value As String)
            _ProductCode = value
        End Set
    End Property

    Public Property OrderNo() As String
        Get
            Return _OrderNo
        End Get
        Set(ByVal value As String)
            _OrderNo = value
        End Set
    End Property


    Public Property ScanDatetimeShow() As String
        Get
            Return _ScanDatetimeShow
        End Get
        Set(ByVal value As String)
            _ScanDatetimeShow = value
        End Set
    End Property

    Public Property ScanDatetime() As Date
        Get
            Return _ScanDatetime
        End Get
        Set(ByVal value As Date)
            _ScanDatetime = value
        End Set
    End Property

    Public Property RackID() As String
        Get
            Return _RackID
        End Get
        Set(ByVal value As String)
            _RackID = value
        End Set
    End Property


End Class
