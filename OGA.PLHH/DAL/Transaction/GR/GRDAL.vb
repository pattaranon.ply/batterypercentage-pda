Imports System.IO
Imports System.Xml
Imports System.Data


Public Class GRDAL


    Public Function getAllData() As DataSet
        Try
            If File.Exists("tbt_GRScan.xml") = False Then
                Dim dt As New DataTable("tbt_GRScan")
                'Create XML Config for config
                With dt
                    .Columns.Add(New DataColumn("RackID", GetType(String)))
                    .Columns.Add(New DataColumn("ScanDatetime", GetType(String)))
                    .Columns.Add(New DataColumn("ScanDatetimeShow", GetType(String)))
                End With

                Dim ds As New DataSet
                ds.Tables.Add(dt)
                ds.WriteXml("tbt_GRScan.xml", XmlWriteMode.WriteSchema)
                Return ds
            Else
                'Read XML GR Data
                Dim ds As New DataSet
                ds.ReadXml("tbt_GRScan.xml")
                Return ds
            End If
        Catch ex As Exception
            Throw New Exception("Get all data error !" & vbCrLf & ex.ToString)
        End Try
    End Function

    Public Function insertData(ByVal GREnt As GREntity) As Boolean
        Try

            Dim dr As DataRow

            dr = GlobalVariable.dsGR.Tables("tbt_GRScan").NewRow
            dr("RackID") = GREnt.RackID
            'dr("ScanDatetime") = Format(GREnt.ScanDatetime, "yyyyMMdd HH:mm:ss")
            dr("ScanDatetime") = Format(GREnt.ScanDatetime, "yyyyMMdd-HHmmss")
            dr("ScanDatetimeShow") = GREnt.ScanDatetimeShow

            GlobalVariable.dsGR.Tables(0).Rows.Add(dr)
            GlobalVariable.dsGR.Tables(0).AcceptChanges()

            GlobalVariable.dsGR.WriteXml("tbt_GRScan.xml")

            Return True

        Catch ex As Exception
            Throw New Exception("Insert data error !" & vbCrLf & ex.ToString)
        End Try
    End Function

    Public Function writeForDelete() As Boolean
        Try
            If GlobalVariable.dsGR.Tables(0).Rows.Count = 0 Then
                GlobalVariable.dsGR.WriteXml("tbt_GRScan.xml", XmlWriteMode.WriteSchema)
            Else
                GlobalVariable.dsGR.WriteXml("tbt_GRScan.xml")
            End If
            Return True

        Catch ex As Exception
            Throw New Exception("Delete data error !" & vbCrLf & ex.ToString)
        End Try
    End Function


    Public Function deleteAllData() As Boolean
        Try


            GlobalVariable.dsGR.Tables("tbt_GRScan").Rows.Clear()

            GlobalVariable.dsGR.WriteXml("tbt_GRScan.xml", XmlWriteMode.WriteSchema)
            Return True

        Catch ex As Exception
            Throw New Exception("Delete all data error !" & vbCrLf & ex.ToString)
        End Try
    End Function

End Class
