Imports System.IO
Imports System.Xml
Imports System.Data


Public Class GIDAL


    Public Function getAllData() As DataSet
        Try
            If File.Exists("tbt_GIScan.xml") = False Then
                Dim dt As New DataTable("tbt_GIScan")
                'Create XML Config for config
                With dt
                    .Columns.Add(New DataColumn("RackID", GetType(String)))
                    .Columns.Add(New DataColumn("ScanDatetime", GetType(String)))
                    .Columns.Add(New DataColumn("ScanDatetimeShow", GetType(String)))
                    .Columns.Add(New DataColumn("OrderNo", GetType(String)))
                    .Columns.Add(New DataColumn("ProductCode", GetType(String)))
                    .Columns.Add(New DataColumn("CustomerCode", GetType(String)))
                End With

                Dim ds As New DataSet
                ds.Tables.Add(dt)
                ds.WriteXml("tbt_GIScan.xml", XmlWriteMode.WriteSchema)
                Return ds
            Else
                'Read XML GI Data
                Dim ds As New DataSet
                ds.ReadXml("tbt_GIScan.xml")
                Return ds
            End If
        Catch ex As Exception
            Throw New Exception("Get all data error !" & vbCrLf & ex.ToString)
        End Try
    End Function

    Public Function insertData(ByVal GIEnt As GIEntity) As Boolean
        Try

            Dim dr As DataRow

            dr = GlobalVariable.dsGI.Tables("tbt_GIScan").NewRow
            dr("RackID") = GIEnt.RackID
            ' dr("ScanDatetime") = Format(GIEnt.ScanDatetime, "yyyyMMdd HH:mm:ss")
            dr("ScanDatetime") = Format(GIEnt.ScanDatetime, "yyyyMMdd-HHmmss")
            dr("ScanDatetimeShow") = GIEnt.ScanDatetimeShow
            dr("OrderNo") = GIEnt.OrderNo
            dr("ProductCode") = GIEnt.ProductCode
            dr("CustomerCode") = GIEnt.CustomerCode

            GlobalVariable.dsGI.Tables(0).Rows.Add(dr)
            GlobalVariable.dsGI.Tables(0).AcceptChanges()

            GlobalVariable.dsGI.WriteXml("tbt_GIScan.xml")

            Return True

        Catch ex As Exception
            Throw New Exception("Insert data error !" & vbCrLf & ex.ToString)
        End Try
    End Function

    Public Function writeForDelete() As Boolean
        Try
            If GlobalVariable.dsGI.Tables(0).Rows.Count = 0 Then
                GlobalVariable.dsGI.WriteXml("tbt_GIScan.xml", XmlWriteMode.WriteSchema)
            Else
                GlobalVariable.dsGI.WriteXml("tbt_GIScan.xml")
            End If
            Return True

        Catch ex As Exception
            Throw New Exception("Delete data error !" & vbCrLf & ex.ToString)
        End Try
    End Function

    Public Function deleteAllData() As Boolean
        Try

            GlobalVariable.dsGI.Tables("tbt_GIScan").Rows.Clear()

            GlobalVariable.dsGI.WriteXml("tbt_GIScan.xml", XmlWriteMode.WriteSchema)
            Return True

        Catch ex As Exception
            Throw New Exception("Delete all data error !" & vbCrLf & ex.ToString)
        End Try
    End Function

End Class
