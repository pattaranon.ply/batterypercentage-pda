Imports System.Data
Imports System.Xml
Imports System.IO


Public Class ConfigFileDAL

    Public Sub getXMLConfigValue()
        Try
            Cursor.Current = Cursors.WaitCursor
            Dim dt As New DataTable("tbs_ConfigWS")
            If File.Exists("tbs_ConfigWS.xml") = False Then
                'Create XML Config for config
                With dt
                    .Columns.Add(New DataColumn("ConfigCode", GetType(String)))
                    .Columns.Add(New DataColumn("ConfigValue", GetType(String)))
                End With
                Dim dr As DataRow
                'dr = dt.NewRow
                'dr("ConfigCode") = "IPServer"
                'dr("ConfigValue") = GlobalVariable.InitIPServer
                'dt.Rows.Add(dr)

                dr = dt.NewRow
                dr("ConfigCode") = "UrlWS"
                dr("ConfigValue") = GlobalVariable.InitUrlWS
                dt.Rows.Add(dr)

                dt.WriteXml("tbs_ConfigWS.xml")

                Dim tmp() As String = GlobalVariable.InitUrlWS.Split("/")
                If tmp.Length >= 2 Then
                    GlobalVariable.IPServer = tmp(2)
                End If

                GlobalVariable.WSOtherURL = GlobalVariable.InitUrlWS
                'GlobalVariable.IPServer = GlobalVariable.InitIPServer
            Else
                'Read XML Config value for IP Server
                Dim ds As New DataSet
                ds.ReadXml("tbs_ConfigWS.xml")
                If ds.Tables(0).Rows.Count <> 0 Then
                    For Each dr As DataRow In ds.Tables(0).Rows

                        If dr("ConfigCode") = "UrlWS" Then
                            GlobalVariable.WSOtherURL = ds.Tables(0).Rows(0).Item("ConfigValue").ToString.Trim
                            'Example : "http://172.23.112.2/WSBarcode/OtherService.asmx"
                            Dim tmp() As String = GlobalVariable.WSOtherURL.Split("/")
                            If tmp.Length >= 2 Then
                                GlobalVariable.IPServer = tmp(2)
                            End If
                        End If

                    Next

                Else
                    OGAMsg.ShowValidate("Not found data" & vbCrLf & "in file tbs_ConfigWS.xml")
                End If
            End If

        Catch ex As Exception
            OGAMsg.ShowErrorTryCatch("Function :" & vbCrLf & "getXMLConfigValue error !", ex)
        Finally
            Cursor.Current = Cursors.Default
        End Try
    End Sub

    Public Sub writeIPServer(ByVal newIP As String)
        Try
            If File.Exists("tbs_ConfigWS.xml") = True Then
                Dim ds As New DataSet
                ds.ReadXml("tbs_ConfigWS.xml")

                If ds.Tables(0).Rows.Count <> 0 Then
                    For Each dr As DataRow In ds.Tables(0).Rows
                        If dr("ConfigCode") = "UrlWS" Then
                            dr.BeginEdit()
                            dr("ConfigValue") = newIP
                            dr.EndEdit()
                            ds.Tables(0).AcceptChanges()
                            ds.WriteXml("tbs_ConfigWS.xml")
                        End If
                    Next

                Else
                    OGAMsg.ShowValidate("Not found data" & vbCrLf & "in file tbs_ConfigWS.xml")
                End If
            End If
        Catch ex As Exception
            OGAMsg.ShowErrorTryCatch("Function :" & vbCrLf & "writeIPServer error !", ex)
        End Try
    End Sub

End Class
